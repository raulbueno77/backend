package jwtManager

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"log"
	"strings"
	"time"
)

type JwtManager struct {
	secretKey		string
	tokenDuration 	time.Duration
}

type Claims struct {
	jwt.StandardClaims
	Id		int32
	Name	string
	Role	string
	Email	string
}

func NewJwtManager (phrase string, tokenDuration time.Duration) (*JwtManager) {
	return &JwtManager{secretKey:phrase, tokenDuration:tokenDuration}
}

func (j *JwtManager) GenerateToken (username string, iduser int32, role string, email string) (*string, error) {

	claims := Claims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(j.tokenDuration).Unix(),
		},
		Id:   	iduser,
		Name: 	strings.Trim(username," "),
		Role: 	role,
		Email:	email,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(j.secretKey))

	if err != nil {
		return nil, err
	}

	return &t, nil
}

func (j *JwtManager) Verify (accesstoken string) (*Claims, error) {
	token, err := jwt.ParseWithClaims(
		accesstoken,
		&Claims{},
		func (token *jwt.Token) (interface{}, error) {
			_, ok := token.Method.(*jwt.SigningMethodHMAC)
			if !ok {
				return nil, fmt.Errorf("Invalid token signing method")
			}

			return []byte(j.secretKey), nil
		},
	)

	if err != nil {
		log.Println(err)
		return nil, fmt.Errorf("Invalid token")
	}

	claims, ok := token.Claims.(*Claims)

	if !ok {
		return nil, fmt.Errorf("Invalid Claims")
	}

	return claims, nil
}


