package database

import (
	model "ceginfor/cehotel-server/models"
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func RegisterBDD(db chan gorm.DB, connected bool, bddType string, host string, port string, user string, passwd string, bdd string) {
	cstr := ""
	switch bddType {
	case "mysql":
		cstr = user + ":" + passwd + "@tcp(" + host + ":" + port + ")/" + bdd + "?charset=utf8&parseTime=True&loc=Local"
		break;
	case "postgres":
		cstr = "host=" + host + " port=" + port + " user=" + user + " dbname=" + bdd + " password=" + passwd + " sslmode=disable"
		break;
	}

	for {
		database, err := gorm.Open(bddType, cstr)
		if err == nil {
			migrate(database)
			indexes(database)
			foreing_keys(database)
			checkData(database)
			db <- *database
			connected = true
			break
		}
	}
}

func migrate(db *gorm.DB) {
	db.AutoMigrate(&model.Users{})
	db.AutoMigrate(&model.UserGroups{})
	db.AutoMigrate(&model.Hotel{})
	db.AutoMigrate(&model.Attribute{})
	db.AutoMigrate(&model.RoomType{})
	db.AutoMigrate(&model.RoomPicture{})
	db.AutoMigrate(&model.RoomPrice{})
	db.AutoMigrate(&model.Room{})
	db.AutoMigrate(&model.Client{})
	db.AutoMigrate(&model.Booking{})
	db.AutoMigrate(&model.BookingDate{})
	db.AutoMigrate(&model.Product{})
	db.AutoMigrate(&model.RoomService{})
	db.AutoMigrate(&model.TaskType{})
	db.AutoMigrate(&model.Task{})
	db.AutoMigrate(&model.PaymentMethod{})
	db.AutoMigrate(&model.ExpenseType{})
	db.AutoMigrate(&model.Expense{})
	db.AutoMigrate(&model.Income{})
	db.AutoMigrate(&model.SaleInvoice{})
	db.AutoMigrate(&model.SaleInvoiceDetail{})
	db.AutoMigrate(&model.Provider{})
	db.AutoMigrate(&model.Guest{})
	db.AutoMigrate(&model.Vat{})
	db.AutoMigrate(&model.SerialInvoice{})
	db.AutoMigrate(&model.Bank{})
}

func indexes(db *gorm.DB) {
	db.Model(&model.Users{}).AddUniqueIndex("email", "email")
	db.Model(&model.Provider{}).AddIndex("provider_document", "document")
	db.Model(&model.Client{}).AddIndex("client_document", "document")
}

func foreing_keys (db *gorm.DB) {
	db.Model(&model.Users{}).AddForeignKey("user_group_id", "user_groups(id)", "restrict", "restrict")
	db.Model(&model.Room{}).AddForeignKey("type_id", "room_types(id)", "restrict", "restrict")
	db.Model(&model.Room{}).AddForeignKey("hotel_id", "hotels(id)", "restrict", "restrict")
	db.Model(&model.RoomPicture{}).AddForeignKey("room_type_id", "room_types(id)", "cascade", "cascade")
	db.Model(&model.RoomPrice{}).AddForeignKey("room_type_id", "room_types(id)", "cascade", "cascade")
	db.Model(&model.Booking{}).AddForeignKey("room_id", "rooms(id)", "restrict", "restrict")
	db.Model(&model.Booking{}).AddForeignKey("client_id", "clients(id)", "restrict", "restrict")
	db.Model(&model.BookingDate{}).AddForeignKey("booking_id", "bookings(id)", "cascade", "cascade")
	db.Model(&model.RoomService{}).AddForeignKey("booking_id", "bookings(id)", "restrict", "restrict")
	db.Model(&model.RoomService{}).AddForeignKey("room_id", "rooms(id)", "restrict", "restrict")
	db.Model(&model.Task{}).AddForeignKey("task_type_id", "task_types(id)", "restrict", "restrict")
	db.Model(&model.Expense{}).AddForeignKey("payment_method_id", "payment_methods(id)", "restrict", "restrict")
	db.Model(&model.Expense{}).AddForeignKey("expense_type_id", "expense_types(id)", "restrict", "restrict")
	db.Model(&model.Income{}).AddForeignKey("payment_method_id", "payment_methods(id)", "restrict", "restrict")
	db.Model(&model.Income{}).AddForeignKey("sale_invoice_id", "sale_invoices(id)", "restrict", "restrict")
	db.Model(&model.SaleInvoiceDetail{}).AddForeignKey("sale_invoice_id", "sale_invoices(id)", "cascade", "cascade")
	db.Model(&model.SaleInvoice{}).AddForeignKey("client_id", "clients(id)", "restrict", "restrict")
	db.Model(&model.Guest{}).AddForeignKey("booking_id", "bookings(id)", "restrict", "restrict")
	db.Model(&model.SaleInvoice{}).AddForeignKey("serial_invoice_id", "serial_invoices(id)", "restrict", "restrict")
	db.Model(&model.Income{}).AddForeignKey("bank_id", "banks(id)", "restrict", "restrict")
	db.Model(&model.Expense{}).AddForeignKey("bank_id", "banks(id)", "restrict", "restrict")
}

func checkData(db *gorm.DB) {
	groups := []model.UserGroups{}

	if err := db.Find(&groups).Error; err != nil {
		fmt.Printf("Error with database: %v\n", err)
	}

	if len(groups) == 0 {
		gr := &model.UserGroups{
			Name:     "Admin",
			Created:  time.Now(),
			Modified: time.Now(),
		}

		db.Create(gr)

		gr = nil

		gr = &model.UserGroups{
			Name:     "Users",
			Created:  time.Now(),
			Modified: time.Now(),
		}

		db.Create(gr)

	}

	users := []model.Users{}

	if err := db.Find(&users).Error; err != nil {
		fmt.Printf("Error with database: %v\n", err)
	}

	if len(users) == 0 {
		user := &model.Users{
			Username:    "admin",
			Firstname:   "Administrador",
			Lastname:    "--",
			Email:       "admin@localhost",
			Archived:    false,
			UserGroupId: 1,
			Created:     time.Now(),
			Modified:    time.Now(),
			Expiration:  nil,
			Description: "",
		}
		user.SetPassword("adminpass1")

		db.Create(user)

	}

	hotels := []model.Hotel{}

	if err := db.Find(&hotels).Error; err != nil {
		fmt.Printf("Error with database: %v\n", err)
	}

	if len(hotels) == 0 {
		hotel := model.Hotel{
			Name:       "My Hotel",
			Document:   "---",
			Reference:  "---",
			Phone:      "",
			Email:      "",
			Address:    "",
			PostalCode: "",
			Country:    "",
			Region:     "",
			City:       "",
			Created:    time.Now(),
			Modified:   time.Now(),
		}

		db.Create(&hotel)
	}

	vats := []model.Vat{}

	if err := db.First(&vats).Error; err != nil {
		fmt.Printf("Error with database: %v\n", err)
	}

	if len(vats) == 0 {
		vat1 := model.Vat{
			Name:  "IVA Normal",
			Value: 21,
		}
		vat2 := model.Vat{
			Name:  "IVA Reducido",
			Value: 10,
		}
		vat3 := model.Vat{
			Name:  "IVA Super Reducido",
			Value: 4,
		}

		db.Create(&vat1)
		db.Create(&vat2)
		db.Create(&vat3)
	}

}
