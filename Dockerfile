FROM golang:1.16
MAINTAINER RGB

# Set GOPATH/GOROOT environment variables
RUN mkdir -p /go
ENV GOPATH /go
ENV PATH $GOPATH/bin:$PATH
ENV GO111MODULE=auto


# go get all of the dependencies
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

# Set up app
WORKDIR /go/src/ceginfor/cehotel-server

EXPOSE 50051