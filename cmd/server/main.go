package main

import (
	"log"
	"os"
	"os/signal"
	"time"

	"github.com/jinzhu/gorm"

	"ceginfor/cehotel-server/database"
	"ceginfor/cehotel-server/services/server"

	"gopkg.in/gcfg.v1"
)

type Config struct {
	Database struct {
		Type     string
		Host     string
		Port     string
		User     string
		Password string
		Name     string
	}
	Jwt struct {
		Key           string
		TokenDuration int32
	}
	Grpc struct {
		Port      string
		Paginator int32
		MaxUploadSize float32
	}
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	//connect with database
	var cfg Config

	err := gcfg.ReadFileInto(&cfg, "./config/config.gcfg")
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Connecting with database server...")
	db := gorm.DB{}
	dbb := make(chan gorm.DB)
	timeout := time.After(time.Second * 30)
	connected := false
	go database.RegisterBDD(dbb, connected, cfg.Database.Type, cfg.Database.Host, cfg.Database.Port, cfg.Database.User, cfg.Database.Password, cfg.Database.Name)

	select {
	case db = <-dbb:
		log.Println("Connected with database server!!!")
		close(dbb)
	case <-timeout:
		if !connected {
			log.Println("Time exceed trying to connect with mysql server! Closing program.")
			log.Println("Goodbye!!!")
			return
		}
	}

	defer db.Close()

	gserver := server.NewGRPCServer(cfg.Grpc.Port, &db, cfg.Jwt.Key, cfg.Jwt.TokenDuration, cfg.Grpc.Paginator, cfg.Grpc.MaxUploadSize)

	//starting GRPC server
	go func() {
		gserver.StartServer()
	}()

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	//Block until the signal is recieved
	<-ch
	gserver.StopServer()

	log.Println("Goodbye!!!!!")
}
