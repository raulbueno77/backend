package interceptor

import (
	"context"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"log"
	jwtManager "ceginfor/cehotel-server/jwt"
	"strings"
)

type AuthInterceptor struct {
	jwtManager		*jwtManager.JwtManager
	accessibleRoles	map[string][]string
	db 				*gorm.DB
}

func NewAuthInterceptor(jwtManager *jwtManager.JwtManager, accessibleRoles map[string][]string, db *gorm.DB) *AuthInterceptor {
	return &AuthInterceptor{jwtManager, accessibleRoles, db}
}

// Unary returns a server interceptor function to authenticate and authorize unary RPC
func (interceptor *AuthInterceptor) Unary() grpc.UnaryServerInterceptor {
	return func (ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		log.Println("---> unary interceptor: ", info.FullMethod)

		err := interceptor.authorize(ctx, info.FullMethod)
		if err != nil {
			log.Println("No permissions")
			return nil, err
		}

		return handler(ctx, req)
	}
}

func (interceptor *AuthInterceptor) Stream() grpc.StreamServerInterceptor {
	return func (srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		log.Println("---> unary interceptor: ", info.FullMethod)

		err := interceptor.authorize(stream.Context(), info.FullMethod)
		if err != nil {
			return err
		}

		return handler(srv, stream)
	}
}

func (interceptor *AuthInterceptor) authorize(ctx context.Context, method string) error {
	accessibleRoles, ok := interceptor.accessibleRoles[method]
	if !ok {
		// everyone can access
		return nil
	}

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return status.Errorf(codes.Unauthenticated, "metadata is not provided")
	}

	values := md["authorization"]
	if len(values) == 0 {
		return status.Errorf(codes.Unauthenticated, "authorization token is not provided")
	}

	accessToken := values[0]
	claims, err := interceptor.jwtManager.Verify(accessToken)
	if err != nil {
		return status.Errorf(codes.Unauthenticated, "access token is invalid: %v", err)
	}

	if !interceptor.validUser(claims.Id, strings.Trim(claims.Name, " ")) {
		return status.Errorf(codes.Unauthenticated, "user no valid")
	}

	for _, role := range accessibleRoles {
		if role == claims.Role {
			return nil
		}
	}

	return status.Error(codes.PermissionDenied, "no permission to access this RPC")
}

func (interceptor *AuthInterceptor) validUser (id int32, name string) bool {
	type Result struct {
		Id		int32
		Name 	string
	}

	query := "SELECT id, CONCAT(TRIM(firstname), ' ', TRIM(lastname)) as name FROM users WHERE id = ? AND CONCAT(TRIM(firstname), ' ', TRIM(lastname)) = ? AND (expiration > NOW() OR expiration  IS NULL)"

	var result Result

	if err := interceptor.db.Raw(query, id, name).Scan(&result).Error; err != nil {
		return false
	}

	return true
}
