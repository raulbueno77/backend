package roles

func AccessibleRoles() map[string][]string {
	const userServicePath = "/cehotel-server.UserServices/"
	const groupServicePath = "/cehotel-server.UserGroupServices/"
	const hotelServicePath = "/cehotel-server.HotelServices/"
	const attributesServicePath = "/cehotel-server.AttributeServices/"
	const roomServicePath = "/cehotel-server.RoomServices/"
	const clientServicePath = "/cehotel-server.ClientServices/"
	const bookingServicePath = "/cehotel-server.BookingServices/"
	const productServicePath = "/cehotel-server.ProductServices/"
	const paymentMethodServicePath = "/cehotel-server.PaymentMethodServices/"
	const expenseServicePath = "/cehotel-server.ExpenseServices/"
	const incomeServicePath = "/cehotel-server.IncomeServices/"
	const providerServicePath = "/cehotel-server.ProviderServices/"
	const invoiceServicePath = "/cehotel-server.SaleInvoiceServices/"
	const bankServicePath = "/cehotel-server.BankServices/"

	return map[string][]string{
		//users
		userServicePath + "CreateUser": {"Admin"},
		userServicePath + "UpdateUser": {"Admin"},
		userServicePath + "DeleteUser": {"Admin"},
		userServicePath + "GetUser": {"Admin"},
		userServicePath + "FindUsers": {"Admin"},
		userServicePath + "ListUsers": {"Admin"},
		userServicePath + "ChangePassword": {"Admin"},
		userServicePath + "Me": {"Admin", "Users"},
		userServicePath + "ChangeEmail": {"Admin"},
		//groups
		groupServicePath + "CreateUserGroup" : {"Admin"},
		groupServicePath + "UpdateUserGroup" : {"Admin"},
		groupServicePath + "DeleteUserGroup" : {"Admin"},
		groupServicePath + "GetUserGroup" : {"Admin", "Users"},
		groupServicePath + "FindUserGroups" : {"Admin"},
		//hotel
		hotelServicePath + "CreateHotel": {"Admin"},
		hotelServicePath + "UpdateHotel": {"Admin"},
		hotelServicePath + "DeleteHotel": {"Admin"},
		hotelServicePath + "GetHotel": {"Admin", "Users"},
		hotelServicePath + "FindHotel": {"Admin", "Users"},
		//attributes
		attributesServicePath + "CreateAtrribute": {"Admin"},
		attributesServicePath + "UpdateAtrribute": {"Admin"},
		attributesServicePath + "DeleteAtrribute": {"Admin"},
		attributesServicePath + "GetAtrribute": {"Admin", "Users"},
		attributesServicePath + "GetAllAtrributes": {"Admin", "Users"},
		//rooms
		roomServicePath + "CreateRoom": {"Admin"},
		roomServicePath + "UpdateRoom": {"Admin"},
		roomServicePath + "DeleteRoom": {"Admin"},
		roomServicePath + "GetRoom": {"Admin", "Users"},
		roomServicePath + "FindRooms": {"Admin", "Users"},
		roomServicePath + "SetPrices": {"Admin"},
		roomServicePath + "SetSinglePrice": {"Admin"},
		roomServicePath + "GetFreeRooms": {"Admin", "User"},
		//clients
		clientServicePath + "CreateClient": {"Admin", "Users"},
		clientServicePath + "UpdateClient": {"Admin", "Users"},
		clientServicePath + "DeleteClient": {"Admin"},
		clientServicePath + "GetClient": {"Admin", "Users"},
		clientServicePath + "FindClients": {"Admin", "Users"},
		//bookins
		bookingServicePath + "CreateBooking": {"Admin", "Users"},
		bookingServicePath + "UpdateBooking": {"Admin", "Users"},
		bookingServicePath + "DeleteBooking": {"Admin"},
		bookingServicePath + "GetBooking": {"Admin", "Users"},
		bookingServicePath + "FindBookings": {"Admin", "Users"},
		bookingServicePath + "ManageChecking": {"Admin", "Users"},
		bookingServicePath + "CreateGuest": {"Admin", "Users"},
		bookingServicePath + "UpdateGuest": {"Admin", "Users"},
		bookingServicePath + "DeleteGuest": {"Admin", "Users"},
		bookingServicePath + "GetGuest": {"Admin", "Users"},
		bookingServicePath + "FindGuests": {"Admin", "Users"},
		//products
		productServicePath + "CreateProduct": {"Admin", "Users"},
		productServicePath + "UpdateProduct": {"Admin", "Users"},
		productServicePath + "DeleteProduct": {"Admin"},
		productServicePath + "GetProduct": {"Admin", "Users"},
		productServicePath + "FindProducts": {"Admin", "Users"},
		//payment methods
		paymentMethodServicePath + "CreatePaymentMethod": {"Admin"},
		paymentMethodServicePath + "UpdatePaymentMethod": {"Admin"},
		paymentMethodServicePath + "DeletePaymentMethod": {"Admin"},
		paymentMethodServicePath + "GetPaymentMethod": {"Admin", "Users"},
		paymentMethodServicePath + "FindPaymentMethods": {"Admin", "Users"},
		//expenses and expense types
		expenseServicePath + "CreateExpense": {"Admin", "Users"},
		expenseServicePath + "CreateExpenseType": {"Admin"},
		expenseServicePath + "UpdateExpense": {"Admin", "Users"},
		expenseServicePath + "UpdateExpenseType": {"Admin"},
		expenseServicePath + "DeleteExpense": {"Admin"},
		expenseServicePath + "DeleteExpenseType": {"Admin"},
		expenseServicePath + "GetExpense": {"Admin", "Users"},
		expenseServicePath + "GetExpenseType": {"Admin", "Users"},
		expenseServicePath + "FindExpenses": {"Admin", "Users"},
		expenseServicePath + "FindExpenseTypes": {"Admin", "Users"},
		//incomes
		incomeServicePath + "CreateIncome": {"Admin", "Users"},
		incomeServicePath + "UpdateIncome": {"Admin", "Users"},
		incomeServicePath + "DeleteIncome": {"Admin"},
		incomeServicePath + "GetIncome": {"Admin", "Users"},
		incomeServicePath + "FindIncomes": {"Admin", "Users"},
		//providers
		providerServicePath + "CreateProvider": {"Admin", "Users"},
		providerServicePath + "UpdateProvider": {"Admin", "Users"},
		providerServicePath + "DeleteProvider": {"Admin"},
		providerServicePath + "GetProvider": {"Admin", "Users"},
		providerServicePath + "FindProviders": {"Admin", "Users"},
		//sale invoices
		invoiceServicePath + "CreateSaleInvoice": {"Admin", "Users"},
		invoiceServicePath + "UpdateSaleInvoice": {"Admin", "Users"},
		invoiceServicePath + "DeleteSaleInvoice": {"Admin"},
		invoiceServicePath + "GetSaleInvoice": {"Admin", "Users"},
		invoiceServicePath + "FindSaleInvoices": {"Admin", "Users"},
		invoiceServicePath + "NextNumber": {"Admin", "Users"},
		invoiceServicePath + "CreateSerialInvoice": {"Admin", "Users"},
		invoiceServicePath + "UpdateSerialInvoice": {"Admin", "Users"},
		invoiceServicePath + "DeleteSerialInvoice": {"Admin"},
		invoiceServicePath + "GetSerialInvoice": {"Admin", "Users"},
		invoiceServicePath + "FindSerialInvoices": {"Admin", "Users"},
		//banks
		bankServicePath + "CreateBank": {"Admin"},
		bankServicePath + "UpdateBank": {"Admin"},
		bankServicePath + "DeleteBank": {"Admin"},
		bankServicePath + "GetBank": {"Admin", "Users"},
		bankServicePath + "FindBanks": {"Admin", "Users"},
	}
}
