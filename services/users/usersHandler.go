package users

import (
	model "ceginfor/cehotel-server/models"
	userpb "ceginfor/cehotel-server/proto/user"
	"context"
	"fmt"
	"github.com/biezhi/gorm-paginator/pagination"
	"github.com/dgrijalva/jwt-go"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"log"
	"math"
	jwtManager "ceginfor/cehotel-server/jwt"
	"ceginfor/cehotel-server/utils"
	"strconv"
	"time"
)

type user struct {
	srv		*grpc.Server
	db		*gorm.DB
	manager	*jwtManager.JwtManager
	paginator		int32
	securityKey		string
}

func NewUserService (srv *grpc.Server, db *gorm.DB, manager *jwtManager.JwtManager, paginator int32, key string) *user {
	return &user{
		srv:           srv,
		db:            db,
		manager: 	   manager,
		paginator:     paginator,
		securityKey:   key,
	}
}

func (u *user) RegisterService() {
	userpb.RegisterUserServicesServer(u.srv, u)
}

func (u *user) CreateUser(ctx context.Context, req *userpb.CreateUserRequest) (*userpb.CreateUserResponse, error) {
	item := req.GetUser()

	usr := u.toModel(*item)

	usr.SetPassword(usr.Password)
	usr.Created = time.Now()
	usr.Modified = time.Now()


	if err := u.db.Create(&usr).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error mysql %v", err))
	}

	user := toGRPC(usr)
	user.Password = usr.Password

	return &userpb.CreateUserResponse{
		User: user,
	}, nil
}

func (u *user) UpdateUser(ctx context.Context, req *userpb.UpdateUserRequest) (*userpb.UpdateUserResponse, error) {
	item := req.GetUser()

	log.Println(item)
	usr := u.toModel(*item)

	usr2 := model.Users{}

	if err := u.db.First(&usr2, item.GetId()).Error; err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Error mysql %v", err))
	}
	log.Printf("password: %v\n", usr.Password)
	if usr.Password == "" {
		log.Println("Keeping password...")
		usr.Password = usr2.Password
	} else {
		usr.SetPassword(usr.Password)
	}

	log.Println(usr.UserGroupId)
	if usr.UserGroupId == int32(0) {
		log.Printf("Keeping user group")
		usr.UserGroupId = usr2.UserGroupId
	}

	usr.Created = usr2.Created
	usr.Modified = time.Now()

	u.db.Exec("DELETE FROM user_hotels WHERE users_id = ?", usr.Id)

	if err := u.db.Save(&usr).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error mysql %v", err))
	}

	user := toGRPC(usr)
	user.Password = usr.Password

	return &userpb.UpdateUserResponse{
		User: user,
	}, nil
}

func (u *user) DeleteUser(ctx context.Context, req *userpb.DeleteUserRequest) (*userpb.DeleteUserResponse, error) {
	id := req.GetId()

	user := model.Users{}

	if err := u.db.First(&user, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Error mysql %v", err))
	}

	if err := u.db.Unscoped().Delete(&user).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error mysql %v", err))
	}

	return &userpb.DeleteUserResponse{
		Message: "User deleted",
	}, nil
}

func (u *user) GetUser(ctx context.Context, req *userpb.GetUserRequest) (*userpb.GetUserResponse, error) {
	id := req.GetId()

	user := model.Users{}

	if err := u.db.Debug().Preload("Hotels").Preload("UserGroup").First(&user, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Error mysql %v", err))
	}

	usr := u.toView(user)
	return &userpb.GetUserResponse{
		User: usr,
	}, nil
}

func (u *user) FindUsers(ctx context.Context, req *userpb.FindUsersRequest) (*userpb.FindUsersResponse, error) {
	filter := req.GetFilter()

	users := []model.Users{}

	if err := u.db.Preload("UserGroup").Where("username like ?", "%"+filter+"%").Or("firstname like ?", "%"+filter+"%").Or("lastname like ?", "%"+filter+"%").Or("email like ?", "%"+filter+"%").Find(&users).Error; err != nil {
		return nil, status.Errorf(codes.Internal, fmt.Sprintf("Error mysql %v", err))
	}

	usr := []*userpb.UserResponse{}

	for _, user := range users {

		usr = append(usr, u.toView(user))
	}

	return &userpb.FindUsersResponse{
		User: usr,
	}, nil
}

func (u *user) Login(ctx context.Context, req *userpb.LoginRequest) (*userpb.LoginResponse, error) {

	user := model.Users{}

	if err := u.db.Debug().Where("username = ?", req.GetUsername()).Preload("Hotels").First(&user).Error; err != nil {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("No username found %v", err))
	}

	if !user.CheckPassword(req.Password) {
		return nil, status.Error(codes.NotFound, "Password incorrect")
	}

	group := model.UserGroups{}

	if err := u.db.First(&group, user.UserGroupId).Error; err != nil {
		return nil, status.Error(codes.NotFound, "No found role")
	}

	//check expiration date
	if user.Expiration != nil {
		now := time.Now()

		difference := now.Sub(*user.Expiration).Hours()

		if difference >= 0 {
			return nil, status.Error(codes.PermissionDenied, "user's permissions expired yet!!!")
		}
	}

	token, err := u.manager.GenerateToken(user.Firstname + " " + user.Lastname, user.Id, group.Name, user.Email)

	if err != nil {
		return nil, status.Error(codes.NotFound, "Error generating token")
	}

	res := &userpb.LoginResponse{
		Message: "Token generated",
		Token:   *token,
	}
	return res, nil
}

func (u *user) ListUsers(ctx context.Context, req *userpb.ListUsersRequest) (*userpb.ListUsersResponse, error) {
	page, err := strconv.Atoi(req.GetPage())
	if err != nil {
		page = 1
	}
	sort := req.GetSort()
	if sort == "" {
		sort = "lastname"
	}
	direction := req.GetDirection()
	if direction == "" {
		direction = "asc"
	}
	group := req.GetGroup()
	active := req.GetActive()

	type total struct {
		Num int
	}

	aux := total{}

	usr := model.Users{}
	sql := usr.QueryListCount(group, active)

	u.db.Raw(sql).Scan(&aux)

	totalRecords := aux.Num

	sql = usr.QueryList(group, active)

	users := []model.UsersList{}

	pagination.Paging(&pagination.Param{
		DB:      u.db.Raw(sql),
		Page:    page,
		Limit:   int(u.paginator),
		OrderBy: []string{sort + " " + direction},
	}, &users)

	result := []*userpb.ListUsers{}

	for _, usr := range users {
		active := true

		if usr.Expiration != nil {
			now := time.Now()
			diff := now.Sub(*usr.Expiration).Hours()
			if diff <= 0 {
				active = false
			}
		}

		result = append(result, &userpb.ListUsers{
			Id:        usr.Id,
			Group:     usr.Group,
			FirstName: usr.Firstname,
			LastName:  usr.Lastname,
			Username:  usr.Username,
			Email:     usr.Email,
			Active:    active,
			Archived:  usr.Archived,
		})
	}

	return &userpb.ListUsersResponse{
		Users:      result,
		TotalPages: int32(math.Ceil(float64(totalRecords) / float64(u.paginator))),
	}, nil
}

func (u *user) ChangePassword(ctx context.Context, req *userpb.ChangePasswordRequest) (*userpb.ChangePasswordResponse, error) {
	id := req.GetId()
	password := req.GetPassword()

	user := model.Users{}

	if err := u.db.First(&user, id).Error; err != nil {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("Error mysql %v", err))
	}

	if password == "" {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Password invalid"))
	}

	user.SetPassword(password)

	return &userpb.ChangePasswordResponse{
		Msg: "Password changed!!!!",
	}, nil
}

func (u *user) Me(ctx context.Context, req *userpb.MeRequest) (*userpb.MeResponse, error) {
	md, ok := metadata.FromIncomingContext(ctx)

	idUser := ""
	if ok {
		claims := jwt.MapClaims{}
		jwt.ParseWithClaims(md["authorization"][0], claims, func(token *jwt.Token) (interface{}, error) {
			return []byte(u.securityKey), nil
		})

		idUser = fmt.Sprintf("%v", claims["Id"])
	}

	if idUser == "" {
		return nil, status.Error(codes.Internal, "Unknown error")
	}

	user := model.Users{}

	if err := u.db.Preload("UserGroup").Preload("Hotels").First(&user, idUser).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error mysql %v", err))
	}

	created, _ := utils.TimestampProto(user.Created)
	modified, _ :=utils.TimestampProto(user.Modified)

	return &userpb.MeResponse{
		User:                 &userpb.User{
			Id:                   user.Id,
			Username:             user.Username,
			Firstname:            user.Firstname,
			Lastname:             user.Lastname,
			Password:             user.Password,
			Email:                user.Email,
			Archived:             user.Archived,
			UserGroupId:          user.UserGroupId,
			Created:              created,
			Modified:             modified,
			Expiration:           func() *timestamp.Timestamp {
				if user.Expiration == nil {
					return nil
				}else {
					expiration, err := utils.TimestampProto(*user.Expiration)
					if err != nil {
						return  nil
					}else {
						return expiration
					}
				}
			} (),
			Description:          user.Description,
			Hotels: 	 func () []int32 {
				if len(user.Hotels) > 0 {
					hotels := []int32{}

					for _, hotel := range user.Hotels {
						hotels = append(hotels, hotel.Id)
					}
					return hotels
				}
				return nil
			}(),
		},
	}, nil
}

func (u *user) ChangeEmail(ctx context.Context, req *userpb.ChangeEmailRequest) (*userpb.ChangeEmailResponse, error) {
	id := req.GetId()
	email := req.GetEmail()

	user := model.Users{}

	if err := u.db.First(&user, id).Error; err != nil {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("Error mysql %v", err))
	}

	if email == "" {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("Email empty"))
	}

	user.Email = email

	if err := u.db.Save(&user).Error; err != nil {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("Error mysql %v", err))
	}

	return &userpb.ChangeEmailResponse{
		Msg:                  "Email changed!!!",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (u *user) TestConnection(ctx context.Context, req *userpb.TestConnectionRequest) (*userpb.TestConnectionResponse, error) {
	return &userpb.TestConnectionResponse{
		State:                true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (u *user) toModel(user userpb.User) model.Users {
	created, _ := utils.Timestamp(user.GetCreated())
	modified, _ := utils.Timestamp(user.GetModified())

	var expiration *time.Time

	if user.GetExpiration() != nil {
		expiration, _ = utils.Timestamp(user.GetExpiration())
	} else {
		expiration = nil
	}

	return model.Users{
		Id:          user.GetId(),
		Username:    user.GetUsername(),
		Firstname:   user.GetFirstname(),
		Lastname:    user.GetLastname(),
		Password:    user.GetPassword(),
		Email:       user.GetEmail(),
		Archived:    user.GetArchived(),
		UserGroupId: user.GetUserGroupId(),
		Created:     *created,
		Modified:    *modified,
		Expiration:  expiration,
		Description: user.GetDescription(),
		Hotels: 	 func () []*model.Hotel {
			if len(user.Hotels) > 0 {
				hotels := []*model.Hotel{}

				for _, hotel := range user.Hotels {
					hotelDb := model.Hotel{}

					if err := u.db.First(&hotelDb, hotel).Error; err != nil {
						return nil
					}
					log.Println(&hotelDb)
					hotels = append(hotels, &hotelDb)
				}
				return hotels
			}
			return nil
		}(),
	}
}

func toGRPC(user model.Users) *userpb.User {
	created, _ := utils.TimestampProto(user.Created)
	modified, _ := utils.TimestampProto(user.Modified)
	var expiration *timestamp.Timestamp

	if user.Expiration != nil {
		expiration, _ = utils.TimestampProto(*user.Expiration)
	} else {
		expiration = nil
	}

	return &userpb.User{
		Id:          user.Id,
		Username:    user.Username,
		Firstname:   user.Firstname,
		Lastname:    user.Lastname,
		Password:    user.Password,
		Email:       user.Email,
		Archived:    user.Archived,
		UserGroupId: user.UserGroupId,
		Created:     created,
		Modified:    modified,
		Expiration:  expiration,
		Description: user.Description,
		Hotels:      func () []int32 {
			hotels := []int32{}

			for _, hotel := range user.Hotels {
				hotels = append(hotels, hotel.Id)
			}

			return []int32{}
		}(),
	}
}

func (u *user) toView(user model.Users) *userpb.UserResponse {

	//search user group in bdd

	return &userpb.UserResponse{
		Id:          user.Id,
		Username:    user.Username,
		Firstname:   user.Firstname,
		Lastname:    user.Lastname,
		Password:    user.Password,
		Email:       user.Email,
		Archived:    user.Archived,
		UserGroup:   func() *userpb.EntityGroup {
			return &userpb.EntityGroup{
				Id:                   user.UserGroup.Id,
				Name:                 user.UserGroup.Name,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		Created:     user.Created.Format("2006-01-02 15:04:05"),
		Modified:    user.Modified.Format("2006-01-02 15:04:05"),
		Expiration:  func () string {
			if user.Expiration == nil {
				return ""
			}else {
				return user.Expiration.Format("2006-01-02 15:04:05")
			}
		}(),
		Description: user.Description,
		Group: 		 user.UserGroup.Name,
		Hotels: 	 func () []*userpb.HotelViewUser {
			if user.Hotels != nil {
				hotels := []*userpb.HotelViewUser{}

				for _, hotel := range user.Hotels {
					hotels = append(hotels, &userpb.HotelViewUser{
						Id:                   hotel.Id,
						Name:                 hotel.Name,
						XXX_NoUnkeyedLiteral: struct{}{},
						XXX_unrecognized:     nil,
						XXX_sizecache:        0,
					})
				}

				return hotels
			}
			return nil
		}(),
	}
}



