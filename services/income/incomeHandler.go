package income

import (
	model "ceginfor/cehotel-server/models"
	expenseincomepb "ceginfor/cehotel-server/proto/expense_income"
	"context"
	"fmt"
	"github.com/biezhi/gorm-paginator/pagination"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"strconv"
	"time"
)

type income struct {
	srv			*grpc.Server
	db 			*gorm.DB
	paginator 	int32
}

func NewIncomeServices(srv *grpc.Server, db *gorm.DB, paginator int32) *income {
	return &income{
		srv:       srv,
		db:        db,
		paginator: paginator,
	}
}

func (i *income) RegisterServices() {
	expenseincomepb.RegisterIncomeServicesServer(i.srv, i)
}

func (i *income) CreateIncome(ctx context.Context, req *expenseincomepb.CreateIncomeRequest) (*expenseincomepb.CreateIncomeResponse, error) {
	item := req.GetIncome()
	
	income := toModel(item)
	
	income.Created = time.Now()
	income.Modified = time.Now()
	
	if err := i.db.Create(&income).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	if err := i.db.Preload("Bank").Preload("PaymentMethod").First(&income, income.Id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &expenseincomepb.CreateIncomeResponse{
		Income:               toGrpc(income),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (i *income) UpdateIncome(ctx context.Context, req *expenseincomepb.UpdateIncomeRequest) (*expenseincomepb.UpdateIncomeResponse, error) {
	item := req.GetIncome()

	incomeDb := model.Income{}

	if err := i.db.First(&incomeDb, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	income := toModel(item)

	income.Created = incomeDb.Created
	income.Modified = time.Now()

	if err := i.db.Save(&income).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	if err := i.db.Preload("Bank").Preload("PaymentMethod").First(&income, income.Id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &expenseincomepb.UpdateIncomeResponse{
		Income:               toGrpc(income),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (i *income) DeleteIncome(ctx context.Context, req *expenseincomepb.DeleteIncomeRequest) (*expenseincomepb.DeleteIncomeResponse, error) {
	id := req.GetId()

	income := model.Income{}

	if err := i.db.First(&income, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	if err := i.db.Unscoped().Delete(&income).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &expenseincomepb.DeleteIncomeResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (i *income) GetIncome(ctx context.Context, req *expenseincomepb.GetIncomeRequest) (*expenseincomepb.GetIncomeResponse, error) {
	id := req.GetId()

	income := model.Income{}
	
	if err := i.db.Preload("Bank").Preload("PaymentMethod").First(&income, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &expenseincomepb.GetIncomeResponse{
		Income:               toGrpc(income),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (i *income) FindIncomes(ctx context.Context, req *expenseincomepb.FindIncomesRequest) (*expenseincomepb.FindIncomesResponse, error) {
	begin, err := time.Parse("2006-01-02 15:04:05", req.GetDateBegin())
	if err != nil {
		begin, err = time.Parse("2006-01-02 15:04:05", req.GetDateBegin() + " 00:00:00")
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Begining date error: %v", err))
		}
	}

	end, err := time.Parse("2006-01-02 15:04:05", req.GetDateEnd())
	if err != nil {
		end, err = time.Parse("2006-01-02 15:04:05", req.GetDateEnd() + " 23:59:59")
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Ending date error: %v", err))
		}
	}

	methodId := req.GetPaymentMethodId()

	bankId := req.GetBankId()

	filter := req.GetFilter()

	page := req.GetPage()

	if page == 0 {
		page = 1
	}

	sort := req.GetSort()

	if sort == "" {
		sort = "incomes.date"
	}

	direction := req.GetSortDirection()

	if direction == "" {
		direction = "ASC"
	}

	where := "1 = 1"

	if methodId != 0 {
		where += " AND incomes.payment_method_id = " + strconv.Itoa(int(methodId))
	}

	if bankId != 0 {
		where += " AND incomes.bank_id = " + strconv.Itoa(int(bankId))
	}

	where += " AND incomes.description like ? AND incomes.date BETWEEN ? AND ?"

	db := i.db.Table("incomes").Select("incomes.*").Joins("join payment_methods on incomes.payment_method_id = payment_methods.id").Joins("join banks on incomes.bank_id = banls.id").Preload("PaymentMethod").Preload("Bank").Where(where, "%" + filter + "%", begin, end)

	incomes := []model.Income{}

	pag := pagination.Paging(&pagination.Param{
		DB:      db,
		Page:    int(page),
		Limit:   int(i.paginator),
		OrderBy: []string{sort + " " + direction},
		ShowSQL: true,
	}, &incomes)

	result := []*expenseincomepb.IncomeView{}
	totalAmount := float32(0.0)

	for _, inc := range incomes {
		totalAmount += inc.Amount
		result = append(result, toGrpc(inc))
	}

	return &expenseincomepb.FindIncomesResponse{
		Incomes:              result,
		TotalPages:           int32(pag.TotalPage),
		TotalAmount:          totalAmount,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func toModel(inc *expenseincomepb.Income) model.Income {
	return model.Income{
		Id:              inc.GetId(),
		Date:            func () time.Time {
			date, err := time.Parse("2006-01-02 15:04:05", inc.GetDate())

			if err != nil {
				return time.Time{}
			}

			return date
		}(),
		PaymentMethodId: inc.GetPaymentMethodId(),
		SaleInvoiceId: 	 func () *int32 {
			invoiceId := inc.GetInvoiceId()
			if invoiceId == 0 {
				return  nil
			}

			return &invoiceId
		}(),
		Description:     inc.GetDescription(),
		Amount:          inc.GetAmount(),
		BankId: 		 inc.GetBankId(),
	}
}

func toGrpc(inc model.Income) *expenseincomepb.IncomeView {
	return &expenseincomepb.IncomeView{
		Id:                   inc.Id,
		Date:                 inc.Date.Format("2006-01-02 15:04:05"),
		PaymentMethod:        func () *expenseincomepb.PaymentMethodView {
			return &expenseincomepb.PaymentMethodView{
				Id:                   inc.PaymentMethod.Id,
				Name:                 inc.PaymentMethod.Name,
				Created:              inc.PaymentMethod.Created.Format("2006-01-02 15:04:05"),
				Modified:             inc.PaymentMethod.Modified.Format("2006-01-02 15:04:05"),
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		Description:          inc.Description,
		Amount:               inc.Amount,
		Created:              inc.Created.Format("2006-01-02 15:04:05"),
		Modified:             inc.Modified.Format("2006-01-02 15:04:05"),
		Bank: 				  func () *expenseincomepb.BankModel {
			return &expenseincomepb.BankModel{
				Id:                   inc.Bank.Id,
				Name:                 inc.Bank.Name,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}
