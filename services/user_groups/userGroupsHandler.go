package userGroups

import (
	"context"
	"fmt"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	model "ceginfor/cehotel-server/models"
	userGrouppb "ceginfor/cehotel-server/proto/userGroup"
	"ceginfor/cehotel-server/utils"
)

type group struct {
	srv		*grpc.Server
	db		*gorm.DB
}

func NewUserGroupService (srv *grpc.Server, db *gorm.DB) *group {
	return &group{
		srv:           srv,
		db:            db,
	}
}

func (g *group) RegisterService() {
	userGrouppb.RegisterUserGroupServicesServer(g.srv, g)
}

func (g *group) CreateUserGroup(ctx context.Context, req *userGrouppb.CreateUserGroupRequest) (*userGrouppb.CreateUserGroupResponse, error) {
	item := req.GetUserGroup()

	group := toModel(*item)

	if err := g.db.Create(&group).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error mysql %v", err))
	}

	grp := toGRPC(*group)

	return &userGrouppb.CreateUserGroupResponse{
		UserGroup:            grp,
	}, nil
}

func (g *group) UpdateUserGroup(ctx context.Context, req *userGrouppb.UpdateUserGroupRequest) (*userGrouppb.UpdateUserGroupResponse, error) {
	item := req.GetUserGroup()

	group := toModel(*item)

	if err := g.db.Save(&group).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error mysql %v", err))
	}

	grp := toGRPC(*group)

	return &userGrouppb.UpdateUserGroupResponse{
		UserGroup:            grp,
	}, nil
}

func (g *group) DeleteUserGroup(ctx context.Context, req *userGrouppb.DeleteUserGroupRequest) (*userGrouppb.DeleteUserGroupResponse, error) {
	id := req.GetId()

	group := model.UserGroups{}

	if err := g.db.Find(&group, id).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error mysql %v", err))
	}

	if err := g.db.Unscoped().Delete(&group).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error mysql %v", err))
	}

	return &userGrouppb.DeleteUserGroupResponse{
		Message:              "User Group deleted",
	}, nil
}

func (g *group) GetUserGroup(ctx context.Context, req *userGrouppb.GetUserGroupRequest) (*userGrouppb.GetUserGroupResponse, error) {
	id := req.GetId()

	group := model.UserGroups{}

	if err := g.db.Find(&group, id).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error mysql %v", err))
	}

	return &userGrouppb.GetUserGroupResponse{
		UserGroup:            toGRPC(group),
	}, nil
}

func (g *group) FindUserGroups(ctx context.Context, req *userGrouppb.FindUserGroupsRequest) (*userGrouppb.FindUserGroupsResponse, error) {
	filter := req.GetFilter()

	groups := []model.UserGroups{}

	if err := g.db.Where("name like ?", "%" + filter + "%").Find(&groups).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error mysql %v", err))
	}

	grps := []*userGrouppb.UserGroup{}

	for _, group := range groups {
		grps = append(grps, toGRPC(group))
	}

	return &userGrouppb.FindUserGroupsResponse{
		UserGroup:            grps,
	},nil
}

func toModel (group userGrouppb.UserGroup) *model.UserGroups {
	created, _ := utils.Timestamp(group.GetCreated())
	modified, _ := utils.Timestamp(group.GetModified())

	return &model.UserGroups{
		Id:       group.GetId(),
		Name:     group.GetName(),
		Created:  *created,
		Modified: *modified,
	}
}

func toGRPC (group model.UserGroups) *userGrouppb.UserGroup {
	created, _ := utils.TimestampProto(group.Created)
	modified, _ := utils.TimestampProto(group.Modified)

	return &userGrouppb.UserGroup{
		Id:                   group.Id,
		Name:                 group.Name,
		Created:              created,
		Modified:             modified,
	}
}