package client

import (
	model "ceginfor/cehotel-server/models"
	clientpb "ceginfor/cehotel-server/proto/client"
	"context"
	"fmt"
	"github.com/biezhi/gorm-paginator/pagination"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"strings"
	"time"
)

type client struct {
	srv				*grpc.Server
	db				*gorm.DB
	paginator		int32
}

func NewClientService (srv *grpc.Server, db *gorm.DB, paginator int32) *client {
	return &client{
		srv:       srv,
		db:        db,
		paginator: paginator,
	}
}

func (c *client) RegisterService () {
	clientpb.RegisterClientServicesServer(c.srv, c)
}

func (c *client) CreateClient(ctx context.Context, req *clientpb.CreateClientRequest) (*clientpb.CreateClientResponse, error) {
	item := req.GetClient()
	client := toModel(item)

	client.Created = time.Now()
	client.Modified = time.Now()

	if client.Document == nil {
		client.Provisional = true
	}

	if err := c.db.Create(&client).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &clientpb.CreateClientResponse{
		Client:               toGrpc(client),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (c *client) UpdateClient(ctx context.Context, req *clientpb.UpdateClientRequest) (*clientpb.UpdateClientResponse, error) {
	item := req.GetClient()
	
	clientDb := model.Client{}
	
	if err := c.db.First(&clientDb, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	client := toModel(item)
	
	client.Created = clientDb.Created
	client.Modified = time.Now()

	if client.Document == nil {
		client.Provisional = true
	}

	if err := c.db.Save(&client).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &clientpb.UpdateClientResponse{
		Client:               toGrpc(client),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (c *client) DeleteClient(ctx context.Context, req *clientpb.DeleteClientRequest) (*clientpb.DeleteClientResponse, error) {
	id := req.GetId()

	client := model.Client{}

	if err := c.db.First(&client, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	if err := c.db.Unscoped().Delete(&client).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &clientpb.DeleteClientResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (c *client) GetClient(ctx context.Context, req *clientpb.GetClientRequest) (*clientpb.GetClientResponse, error) {
	id := req.GetId()

	client := model.Client{}

	if err := c.db.First(&client, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &clientpb.GetClientResponse{
		Client:               toGrpc(client),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (c *client) FindClients(ctx context.Context, req *clientpb.FindClientsRequest) (*clientpb.FindClientsResponse, error) {
	filter := req.GetFilter()
	page := req.GetPage()
	
	if page == 0 {
		page = 1
	}
	
	sort := req.GetSort()
	
	if sort == "" {
		sort = "id"
	}
	
	direction := req.GetSortDirection()
	
	if direction == "" {
		direction = "asc"
	}

	clients := []model.Client{}

	pag := pagination.Paging(&pagination.Param{
		DB:      c.db.Where("document like ? OR full_name like ? OR country like ? OR region like ? OR city like ? or home_phone like ? or cell_phone like ? or email like ?", "%" + filter + "%", "%" + filter + "%", "%" + filter + "%", "%" + filter + "%", "%" + filter + "%", "%" + filter + "%", "%" + filter + "%", "%" + filter + "%"),
		Page:    int(page),
		Limit:   int(c.paginator),
		OrderBy: []string{sort + " " + direction},
		ShowSQL: true,
	}, &clients)

	result := []*clientpb.ClientView{}

	for _, cli := range clients {
		result = append(result, toGrpc(cli))
	}

	return &clientpb.FindClientsResponse{
		Clients:              result,
		TotalPages:           int32(pag.TotalPage),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (c *client) GetClientByDocument(ctx context.Context, req *clientpb.GetClientByDocumentRequest) (*clientpb.GetClientByDocumentResponse, error) {
	document := req.GetDocument()

	client := model.Client{}

	if err := c.db.First(&client, "document = ?", document).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	return &clientpb.GetClientByDocumentResponse{
		Client:               toGrpc(client),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func toModel (cli *clientpb.Client) model.Client {
	return model.Client{
		Id:                 cli.GetId(),
		FirstName:          cli.GetFirstName(),
		LastName:           func () *string {
			to_return := ""
			if cli.GetLastName() != "" {
				to_return = cli.GetLastName()
				return &to_return
			}
			return nil
		}(),
		SecondLastName:     func () *string {
			to_return := ""
			if cli.GetSecondLastName() != "" {
				to_return = cli.GetSecondLastName()
				return &to_return
			}
			return nil
		}(),
		BirthDate:          func () *time.Time {
			if cli.GetBirthDate() != "" {
				to_return, err := time.Parse("2006-01-02", cli.GetBirthDate())
				if err != nil {
					return nil
				}
				return &to_return
			}
			return nil
		}(),
		Document:           func () *string {
			to_return := ""
			if cli.GetDocument() != "" {
			to_return = cli.GetDocument()
			return &to_return
		}
			return nil
		}(),
		DocumentType:       func () *string {
			to_return := ""
			if cli.GetDocumentType() != "" {
				to_return = cli.GetDocumentType()
				return &to_return
			}
			return nil
		}(),
		Gender:             func () *string {
			to_return := ""
			if cli.GetGender() != "" {
				to_return = cli.GetGender()
				return &to_return
			}
			return nil
		}(),
		Address:            func () *string {
			to_return := ""
			if cli.GetAddress() != "" {
				to_return = cli.GetAddress()
				return &to_return
			}
			return nil
		}(),
		PostalCode:         func () *string {
			to_return := ""
			if cli.GetPostalCode() != "" {
				to_return = cli.GetPostalCode()
				return &to_return
			}
			return nil
		}(),
		Country:            func () *string {
			to_return := ""
			if cli.GetCountry() != "" {
				to_return = cli.GetCountry()
				return &to_return
			}
			return nil
		}(),
		Region:             func () *string {
			to_return := ""
			if cli.GetRegion() != "" {
				to_return = cli.GetRegion()
				return &to_return
			}
			return nil
		}(),
		City:               func () *string {
			to_return := ""
			if cli.GetCity() != "" {
				to_return = cli.GetCity()
				return &to_return
			}
			return nil
		}(),
		HomePhone:          func () *string {
			to_return := ""
			if cli.GetHomePhone() != "" {
				to_return = cli.GetHomePhone()
				return &to_return
			}
			return nil
		}(),
		CellPhone:          func () *string {
			to_return := ""
			if cli.GetCellPhone() != "" {
				to_return = cli.GetCellPhone()
				return &to_return
			}
			return nil
		}(),
		Email:              func () *string {
			to_return := ""
			if cli.GetEmail() != "" {
				to_return = cli.GetEmail()
				return &to_return
			}
			return nil
		}(),
		CreditCardNumber:   func () *string {
			to_return := ""
			if cli.GetCreditCardNumber() != "" {
				to_return = cli.GetCreditCardNumber()
				return &to_return
			}
			return nil
		}(),
		CreditCardMonth:    func () *string {
			to_return := ""
			if cli.GetCreditCardMonth() != "" {
				to_return = cli.GetCreditCardMonth()
				return &to_return
			}
			return nil
		}(),
		CreditCardYear:     func () *string {
			to_return := ""
			if cli.GetCreditCardYear() != "" {
				to_return = cli.GetCreditCardYear()
				return &to_return
			}
			return nil
		}(),
		CreditCardSecurity: func () *string {
			to_return := ""
			if cli.GetCreditCardSecurity() != "" {
				to_return = cli.GetCreditCardSecurity()
				return &to_return
			}
			return nil
		}(),
		Observations:       func () *string {
			to_return := ""
			if cli.GetObservations() != "" {
				to_return = cli.GetObservations()
				return &to_return
			}
			return nil
		}(),
		Provisional:        cli.GetProvisional(),
		Created:            time.Time{},
		Modified:           time.Time{},
		FullName: 			func () string {
			toReturn := cli.GetFirstName() + " " + cli.GetLastName() + " " + cli.GetSecondLastName()
			toReturn = strings.TrimSpace(toReturn)
			return toReturn
		}(),
		DocumentDate: func () *time.Time {
			if cli.GetDocumentDate() != "" {
				to_return, err := time.Parse("2006-01-02", cli.GetDocumentDate())
				if err != nil {
					return nil
				}
				return &to_return
			}
			return nil
		}(),
	}
}

func toGrpc(cli model.Client) *clientpb.ClientView {
	return &clientpb.ClientView{
		Id:                   cli.Id,
		FirstName:            cli.FirstName,
		LastName:             func () string {
			if cli.LastName != nil {
				return *cli.LastName
			}
			return ""
		}(),
		SecondLastName:       func () string {
			if cli.SecondLastName != nil {
				return *cli.SecondLastName
			}
			return ""
		}(),
		BirthDate:            func () string {
			if cli.BirthDate != nil {
				return cli.BirthDate.Format("2006-01-02")
			}
			return ""
		}(),
		Document:             func () string {
			if cli.Document != nil {
				return *cli.Document
			}
			return ""
		}(),
		DocumentType:         func () string {
			if cli.DocumentType != nil {
				return *cli.DocumentType
			}
			return ""
		}(),
		Gender:               func () string {
			if cli.Gender != nil {
				return *cli.Gender
			}
			return ""
		}(),
		Address:              func () string {
			if cli.Address != nil {
				return *cli.Address
			}
			return ""
		}(),
		PostalCode:           func () string {
			if cli.PostalCode != nil {
				return *cli.PostalCode
			}
			return ""
		}(),
		Country:              func () string {
			if cli.Country != nil {
				return *cli.Country
			}
			return ""
		}(),
		Region:               func () string {
			if cli.Region != nil {
				return *cli.Region
			}
			return ""
		}(),
		City:                 func () string {
			if cli.City != nil {
				return *cli.City
			}
			return ""
		}(),
		HomePhone:            func () string {
			if cli.HomePhone != nil {
				return *cli.HomePhone
			}
			return ""
		}(),
		CellPhone:            func () string {
			if cli.CellPhone != nil {
				return *cli.CellPhone
			}
			return ""
		}(),
		Email:                func () string {
			if cli.Email != nil {
				return *cli.Email
			}
			return ""
		}(),
		CreditCardNumber:     func () string {
			if cli.CreditCardNumber != nil {
				return *cli.CreditCardNumber
			}
			return ""
		}(),
		CreditCardMonth:      func () string {
			if cli.CreditCardMonth != nil {
				return *cli.CreditCardMonth
			}
			return ""
		}(),
		CreditCardYear:       func () string {
			if cli.CreditCardYear != nil {
				return *cli.CreditCardYear
			}
			return ""
		}(),
		CreditCardSecurity:   func () string {
			if cli.CreditCardSecurity != nil {
				return *cli.CreditCardSecurity
			}
			return ""
		}(),
		Observations:         func () string {
			if cli.Observations != nil {
				return *cli.Observations
			}
			return ""
		}(),
		DocumentDate: func () string {
			if cli.DocumentDate != nil {
				return cli.DocumentDate.Format("2006-01-02")
			}
			return ""
		}(),
		Provisional:          cli.Provisional,
		Created:              cli.Created.Format("2006-01-02 15:04:05"),
		Modified:             cli.Created.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}