package server

import (
	"ceginfor/cehotel-server/roles"
	"ceginfor/cehotel-server/services/bank"
	"ceginfor/cehotel-server/services/booking"
	"ceginfor/cehotel-server/services/client"
	"ceginfor/cehotel-server/services/expense"
	"ceginfor/cehotel-server/services/hotel"
	"ceginfor/cehotel-server/services/income"
	paymentMethod "ceginfor/cehotel-server/services/payment_method"
	"ceginfor/cehotel-server/services/product"
	"ceginfor/cehotel-server/services/provider"
	"ceginfor/cehotel-server/services/room"
	saleInvoice "ceginfor/cehotel-server/services/sale_invoice"
	"ceginfor/cehotel-server/services/task"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	"ceginfor/cehotel-server/interceptors"
	jwtManager "ceginfor/cehotel-server/jwt"
	userGroups "ceginfor/cehotel-server/services/user_groups"
	"ceginfor/cehotel-server/services/users"
	"time"
)

type GrpcServer struct {
	list 			net.Listener
	srv				*grpc.Server
	db				*gorm.DB
	secretKey		string
	tokenDuration	int32
	manager			*jwtManager.JwtManager
	paginator		int32
	maxUploadSize	float32
	port			string
}

var resolutions = map[string]int {
	"x-small": 	60,
	"small": 	150,
	"medium": 	180,
	"large": 	300,
	"vga": 		480,
	"hd": 		720,
	"fullhd": 	1080,
}

func NewGRPCServer (port string, db *gorm.DB, key string, duration int32, paginator int32, maxUploadSize float32) *GrpcServer {
	lis, err := net.Listen("tcp","0.0.0.0:" + port)

	if err != nil {
		log.Fatalf("Error listening tcp port %v, %v\n", port, err)
		return nil
	}

	tokenDuration := time.Second * time.Duration(duration)

	manager := jwtManager.NewJwtManager(key, tokenDuration)

	interceptor := interceptor.NewAuthInterceptor(manager, roles.AccessibleRoles(), db)

	s := grpc.NewServer(grpc.UnaryInterceptor(interceptor.Unary()), grpc.StreamInterceptor(interceptor.Stream()))
	reflection.Register(s)

	return &GrpcServer{
		list: lis,
		srv:  s,
		db: db,
		secretKey: key,
		manager: manager,
		paginator: paginator,
		port: port,
		maxUploadSize: maxUploadSize,
	}
}

func (gsrv *GrpcServer) StartServer () {
	//register services here
	usr := users.NewUserService(gsrv.srv, gsrv.db, gsrv.manager, gsrv.paginator, gsrv.secretKey)
	usr.RegisterService()

	grp := userGroups.NewUserGroupService(gsrv.srv, gsrv.db)
	grp.RegisterService()

	hot := hotel.NewHotelService(gsrv.srv, gsrv.db, gsrv.maxUploadSize, resolutions)
	hot.RegisterService()

	rm := room.NewRoomService(gsrv.srv, gsrv.db, gsrv.paginator, gsrv.maxUploadSize, resolutions)
	rm.RegisterServices()

	cli := client.NewClientService(gsrv.srv, gsrv.db, gsrv.paginator)
	cli.RegisterService()

	book := booking.NewBookingService(gsrv.srv, gsrv.db, gsrv.paginator)
	book.RegisterService()

	prod := product.NewProductService(gsrv.srv, gsrv.db, gsrv.paginator)
	prod.RegisterService()

	tsk := task.NewTaskServices(gsrv.srv, gsrv.db, gsrv.paginator)
	tsk.RegisterServices()

	pay := paymentMethod.NewPaymentMethodService(gsrv.srv, gsrv.db)
	pay.RegisterServices()

	exp := expense.NewExpenseServices(gsrv.srv, gsrv.db, gsrv.paginator)
	exp.RegisterServices()

	inc := income.NewIncomeServices(gsrv.srv, gsrv.db, gsrv.paginator)
	inc.RegisterServices()

	prov := provider.NewProviderService(gsrv.srv, gsrv.db, gsrv.paginator)
	prov.RegisterServices()

	inv := saleInvoice.NewSaleInvoiceService(gsrv.srv, gsrv.db, gsrv.paginator)
	inv.RegisterServices()

	bnk := bank.NewBankService(gsrv.srv, gsrv.db)
	bnk.RegisterServices()

	log.Println("Starting GRPC Server...")
	go func() {
		if err := gsrv.srv.Serve(gsrv.list); err != nil {
			log.Fatalf("Error starting server %v\n", err)
		}
	}()
	log.Printf("GRPC Server running at port: %v...", gsrv.port)
}

func (gsrv *GrpcServer) StopServer () {
	log.Println("Stoping the GRPC server...")
	gsrv.srv.Stop()
	log.Println("Closing the GRPC listener...")
	gsrv.list.Close()
}