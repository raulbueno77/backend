package room

import (
	"bufio"
	"bytes"
	model "ceginfor/cehotel-server/models"
	roompb "ceginfor/cehotel-server/proto/room"
	"ceginfor/cehotel-server/utils"
	"context"
	"fmt"
	"github.com/biezhi/gorm-paginator/pagination"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type room struct {
	srv				*grpc.Server
	db				*gorm.DB
	paginator		int32
	maxUploadSize	float32
	resolutions		map[string]int
}

func NewRoomService(srv *grpc.Server, db *gorm.DB, paginator int32, maxUploadSize float32, resolutions map[string]int) *room {
	return &room{
		srv:           srv,
		db:            db,
		paginator:     paginator,
		maxUploadSize: maxUploadSize,
		resolutions:   resolutions,
	}
}

func (r *room) RegisterServices() {
	roompb.RegisterAttributeServicesServer(r.srv, r)
	roompb.RegisterRoomServicesServer(r.srv, r)
}

//attributes part
func (r *room) CreateAttribute(ctx context.Context, req *roompb.CreateAttributeRequest) (*roompb.CreateAttributeResponse, error) {
	item := req.GetAtribute()

	attribute := attributeToModel(item)
	attribute.Created = time.Now()
	attribute.Modified = time.Now()

	if err := r.db.Create(&attribute).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprint("Database Error: %v", err))
	}

	return &roompb.CreateAttributeResponse{
		Attribute:            attributeToGrpc(attribute),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) UpdateAttribute(ctx context.Context, req *roompb.UpdateAttributeRequest) (*roompb.UpdateAttributeResponse, error) {
	item := req.GetAtribute()

	attribute := attributeToModel(item)

	attr := model.Attribute{}

	if err := r.db.First(&attr, item.Id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprint("Database Error: %v", err))
	}

	attribute.Created = attr.Created
	attribute.Modified = time.Now()

	if err := r.db.Save(&attribute).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprint("Database Error: %v", err))
	}

	return &roompb.UpdateAttributeResponse{
		Attribute:            attributeToGrpc(attribute),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) DeleteAttribute(ctx context.Context, req *roompb.DeleteAttributeRequest) (*roompb.DeleteAttributeResponse, error) {
	id := req.GetId()

	attribute := model.Attribute{}

	if err := r.db.First(&attribute, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprint("Database Error: %v", err))
	}

	if err := r.db.Unscoped().Delete(&attribute).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprint("Database Error: %v", err))
	}

	return &roompb.DeleteAttributeResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) GetAttribute(ctx context.Context, req *roompb.GetAttributeRequest) (*roompb.GetAttributeResponse, error) {
	id := req.GetId()

	attribute := model.Attribute{}

	if err := r.db.First(&attribute, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprint("Database Error: %v", err))
	}

	return &roompb.GetAttributeResponse{
		Attribute:            attributeToGrpc(attribute),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) GetAllAttributes(ctx context.Context, req *roompb.GetAllAttributesRequest) (*roompb.GetAllAttributesResponse, error) {
	filter := req.GetFilter()

	attributes := []model.Attribute{}

	if err := r.db.Where("name like ?", "%" + filter + "%").Find(&attributes).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprint("Database Error: %v", err))
	}

	result := []*roompb.AttributeView{}

	for _, attr := range attributes {
		result = append(result, attributeToGrpc(attr))
	}

	return &roompb.GetAllAttributesResponse{
		Attribute:            result,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func attributeToModel(attr *roompb.Attribute) model.Attribute {
	return model.Attribute{
		Id:       attr.GetId(),
		Name:     attr.GetName(),
	}
}

func attributeToGrpc(attr model.Attribute) *roompb.AttributeView {
	return &roompb.AttributeView{
		Id:                   attr.Id,
		Name:                 attr.Name,
		Created:              attr.Created.Format("2006-01-02 15:04:05"),
		Modified:             attr.Modified.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}
//end attributes part
func (r *room) CreateRoom(ctx context.Context, req *roompb.CreateRoomRequest) (*roompb.CreateRoomResponse, error) {
	item := req.GetRoom()
	
	room := r.roomToModel(item)
	
	room.Created = time.Now()
	room.Modified = time.Now()
	
	if err := r.db.Debug().Create(&room).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &roompb.CreateRoomResponse{
		Room:                 roomToGrpc(room),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) UpdateRoom(ctx context.Context, req *roompb.UpdateRoomRequest) (*roompb.UpdateRoomResponse, error) {
	item := req.GetRoom()
	
	roomDb := model.Room{}
	
	if err := r.db.First(&roomDb, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}


	r.db.Exec("DELETE FROM attributes_room WHERE room_id = ?", roomDb.Id)

	room := r.roomToModel(item)
	
	room.Created = roomDb.Created
	room.Modified = time.Now()

	if err := r.db.Save(&room).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &roompb.UpdateRoomResponse{
		Room:                 roomToGrpc(room),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) DeleteRoom(ctx context.Context, req *roompb.DeleteRoomRequest) (*roompb.DeleteRoomResponse, error) {
	id := req.GetId()
	
	room := model.Room{}

	if err := r.db.First(&room, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	if err := r.db.Unscoped().Delete(&room).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &roompb.DeleteRoomResponse{
		Status:                 true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) GetRoom(ctx context.Context, req *roompb.GetRoomRequest) (*roompb.GetRoomResponse, error) {
	id := req.GetId()
	name := req.GetName()
	hotelId := req.GetHotelId()

	room := model.Room{}

	if id != 0 {
		if err := r.db.Preload("Hotel").Preload("Type").Preload("Attributes").First(&room, id).Error; err != nil {
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}
	}else {
		if err := r.db.Preload("Hotel").Preload("Type").Preload("Attributes").First(&room, "name = ? AND hotel_id = ?", name, hotelId).Error; err != nil {
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}
	}

	return &roompb.GetRoomResponse{
		Room:                 roomToGrpc(room),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) FindRooms(ctx context.Context, req *roompb.FindRoomsRequest) (*roompb.FindRoomsResponse, error) {

	idHotel := req.GetIdHotel()
	filter := req.GetFtr()
	page := req.GetPage()
	if page == 0 {
		page = 1
	}

	sort := req.GetSort()
	if sort == "" {
		sort = "rooms.name"
	}

	direction := req.GetSortDirection()
	if direction == "" {
		direction = "asc"
	}

	rooms := []model.Room{}

	where := "rooms.hotel_id = " + strconv.Itoa(int(idHotel)) + " AND (rooms.name like '%" + filter + "%' OR room_types.name like '%" + filter + "%')"

	pag := pagination.Paging(&pagination.Param{
		DB:      r.db.Table("rooms").Select("rooms.*").Joins("join hotels on rooms.hotel_id = hotels.id").Joins("join room_types on rooms.type_id = room_types.id").Where(where).Preload("Hotel").Preload("Type"),
		Page:    int(page),
		Limit:   10,
		OrderBy: []string{sort + " " + direction},
		ShowSQL: true,
	}, &rooms)

	result := []*roompb.RoomView{}

	for _, room := range rooms {
		result = append(result, roomToGrpc(room))
	}

	return &roompb.FindRoomsResponse{
		Room:                 result,
		TotalPages:           int32(pag.TotalPage),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) GetFreeRooms(ctx context.Context, req *roompb.GetFreeRoomsRequest) (*roompb.GetFreeRoomsResponse, error) {
	begin, err := time.Parse("2006-01-02", req.GetDateBeging())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Begining date error: %v", err))
	}

	end, err := time.Parse("2006-01-02", req.GetDateEnd())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Begining date error: %v", err))
	}

	hotel_id := req.GetHotelId()

	allRooms := []model.Room{}
	selecteds := []struct {
		Id		int
		Room	string
		Type	string
	}{}

	if err := r.db.Table("rooms").Select("rooms.*").Joins("join hotels on rooms.hotel_id = hotels.id").Joins("join room_types on rooms.type_id = room_types.id").Where("rooms.hotel_id = ?", hotel_id).Preload("Hotel").Preload("Type").Find(&allRooms).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	query := "select r.id, r.name as room, rt.name as type from booking_dates bt " +
		"inner join bookings b on bt.booking_id = b.id " +
		"inner join rooms r on b.room_id = r.id " +
		"inner join room_types rt on r.type_id = rt.id " +
		"where r.hotel_id = " + strconv.Itoa(int(hotel_id)) + " and bt.date BETWEEN '" + begin.Format("2006-01-02") + "' AND '" + end.Format("2006-01-02") + "' " +
		"group by r.id, r.name, rt.name"

	r.db.Raw(query).Scan(&selecteds)
	
	result := []*roompb.RoomView{}
	
	for _, room := range allRooms {
		exists := false
		for _, sel := range selecteds {
			if sel.Id == int(room.Id) {
				exists = true
				break
			}
		}
		
		if !exists {
			result = append(result, roomToGrpc(room))
		}
	}
	
	return &roompb.GetFreeRoomsResponse{
		Rooms:                result,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) roomToModel(room *roompb.Room) model.Room {
	return model.Room{
		Id:         room.GetId(),
		Name:       room.GetName(),
		Floor:      room.GetFloor(),
		Attributes: func () []*model.Attribute {
			if len(room.GetAttributes()) > 0 {
				attributes := []*model.Attribute{}

				for _, rm := range room.GetAttributes() {
					attrDb := model.Attribute{}

					if err := r.db.First(&attrDb, rm).Error; err != nil {
						log.Printf("Error not found: %v", err)
						return nil
					}
					attributes = append(attributes, &attrDb)
				}

				return attributes
			}
			return nil
		}(),
		TypeId:     room.GetIdType(),
		HotelId:    room.IdHotel,
		Disabled:   room.GetDisabled(),
	}
}

func roomToGrpc(room model.Room) *roompb.RoomView {
	return &roompb.RoomView{
		Id:                   room.Id,
		Name:                 room.Name,
		Floor:                room.Floor,
		Attributes:            func () []*roompb.AttributeView {
			if len(room.Attributes) > 0 {
				attributes := []*roompb.AttributeView{}

				for _, attr := range room.Attributes {
					attribute := roompb.AttributeView{
						Id:                   attr.Id,
						Name:                 attr.Name,
						Created:              attr.Created.Format("2006-01-02 15:04:05"),
						Modified:             attr.Modified.Format("2006-01-02 15:04:05"),
						XXX_NoUnkeyedLiteral: struct{}{},
						XXX_unrecognized:     nil,
						XXX_sizecache:        0,
					}

					attributes = append(attributes, &attribute)
				}

				return attributes
			}
			return nil
		}(),
		RoomType:             func () *roompb.RoomTypeViewList {
			return &roompb.RoomTypeViewList{
				Id:                   room.Type.Id,
				Name:                 room.Type.Name,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		Hotel:                func () *roompb.HotelViewList {
			return &roompb.HotelViewList{
				Id:                   room.Hotel.Id,
				Name:                 room.Hotel.Name,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		Disabled:             room.Disabled,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

//prices
func (r *room) GetPrices(ctx context.Context, req *roompb.GetPricesRequest) (*roompb.GetPricesResponse, error) {
	begin, err := time.Parse("2006-01-02", req.GetBegin())

	if err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Format begining date err: %v", err))
	}

	end, err := time.Parse("2006-01-02", req.GetEnd())

	if err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Format begining date err: %v", err))
	}

	roomId := req.GetRoomId()

	prices := []*roompb.Price{}

	currentDate := begin

	for {
		if currentDate.Unix() > end.Unix() {
			break
		}

		price := model.RoomPrice{}

		if err := r.db.First(&price, "room_type_id = ? AND date = ?", roomId, currentDate).Error; err == nil {
			prices = append(prices, &roompb.Price{
				RoomTypeId:           roomId,
				Date:                 price.Date.Format("2006-01-02"),
				Price:                price.Price,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			})
		}else {
			roomType := model.RoomType{}

			if err := r.db.First(&roomType, roomId).Error; err == nil {
				prices = append(prices, &roompb.Price{
					RoomTypeId:           roomId,
					Date:                 currentDate.Format("2006-01-02"),
					Price:                roomType.BasePrice,
					XXX_NoUnkeyedLiteral: struct{}{},
					XXX_unrecognized:     nil,
					XXX_sizecache:        0,
				})
			}else {
				prices = append(prices, &roompb.Price{
					RoomTypeId:           roomId,
					Date:                 currentDate.Format("2006-01-02"),
					Price:                0,
					XXX_NoUnkeyedLiteral: struct{}{},
					XXX_unrecognized:     nil,
					XXX_sizecache:        0,
				})
			}
		}
	}


	return &roompb.GetPricesResponse{
		Prices:               prices,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) GetSinglePrice(ctx context.Context, req *roompb.GetSinglePriceRequest) (*roompb.GetSinglePriceResponse, error) {
	return nil, nil
}

func (r *room) SetPrices(ctx context.Context, req *roompb.SetPricesRequest) (*roompb.SetPricesResponse, error) {
	room_id := req.GetRoomTypeId()
	begin, err := time.Parse("2006-01-02", req.GetBegin())
	price := req.GetPrice()

	if err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Format begining date err: %v", err))
	}

	end, err := time.Parse("2006-01-02", req.GetEnd())

	if err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Format begining date err: %v", err))
	}

	diff := end.Sub(begin)

	counter := diff.Hours()

	for counter >= 0 {
		roomPrice := model.RoomPrice{}
		
		if err := r.db.Where("date = ? AND room_type_id = ?", begin, room_id).First(&roomPrice).Error; err != nil {
			roomPrice = model.RoomPrice{
				Date:       begin,
				Price:      price,
				RoomTypeId: room_id,
				Created:    time.Now(),
				Modified:   time.Now(),
			}
			r.db.Create(&roomPrice)
		}else {
			roomPrice.Price = price
			roomPrice.Modified = time.Now()
			r.db.Save(&roomPrice)
		}

		begin = begin.Add(time.Hour * 24)
		counter -= 24
	}
	
	return &roompb.SetPricesResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) SetSinglePrice(ctx context.Context, req *roompb.SetSinglePriceRequest) (*roompb.SetSinglePriceResponse, error) {
	item := req.GetPrice()

	room_type_id := item.GetRoomTypeId()
	date, err := time.Parse("2006-01-02", item.GetDate())

	if err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Format begining date err: %v", err))
	}

	price := model.RoomPrice{}

	if err := r.db.Where("room_type_id = ? AND date = ?", room_type_id, date).First(&price).Error; err != nil {
		price = model.RoomPrice{
			Date:       date,
			Price:      item.GetPrice(),
			RoomTypeId: room_type_id,
			Created:    time.Now(),
			Modified:   time.Now(),
		}
		r.db.Create(&price)
	}else {
		price.Price = item.GetPrice()
		price.Modified = time.Now()
		r.db.Save(&price)
	}

	return &roompb.SetSinglePriceResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) CreateRoomType(ctx context.Context, req *roompb.CreateRoomTypeRequest) (*roompb.CreateRoomTypeResponse, error) {
	item := req.GetRoomType()

	roomType := roomTypeToModel(item)
	
	roomType.Created = time.Now()
	roomType.Modified = time.Now()
	
	if err := r.db.Create(&roomType).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &roompb.CreateRoomTypeResponse{
		RoomType:             roomTypeToGrpc(roomType),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) UpdateRoomType(ctx context.Context, req *roompb.UpdateRoomTypeRequest) (*roompb.UpdateRoomTypeResponse, error) {
	item := req.GetRoomType()
	
	roomTypeDB := model.RoomType{}
	
	if err := r.db.First(&roomTypeDB, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	roomType := roomTypeToModel(item)

	roomType.Created = roomTypeDB.Created
	roomType.Modified = time.Now()

	if err := r.db.Save(&roomType).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &roompb.UpdateRoomTypeResponse{
		RoomType:             roomTypeToGrpc(roomType),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) DeleteRoomType(ctx context.Context, req *roompb.DeleteRoomTypeRequest) (*roompb.DeleteRoomTypeResponse, error) {
	id := req.GetRoomTypeId()
	
	roomType := model.RoomType{}
	
	if err := r.db.First(&roomType, id).Preload("Pictures").Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	for _, picture := range roomType.Pictures {
		os.Remove("media/" + picture.File)
	}
	
	if err := r.db.Unscoped().Delete(&roomType).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &roompb.DeleteRoomTypeResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) GetRoomType(ctx context.Context, req *roompb.GetRoomTypeRequest) (*roompb.GetRoomTypeResponse, error) {
	id := req.GetRoomTypeId()

	roomType := model.RoomType{}

	if err := r.db.First(&roomType, id).Preload("Pictures").Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	pictures := []model.RoomPicture{}
	picturesView := []*roompb.PictureRoomData{}

	if err := r.db.Order("id ASC").Find(&pictures, "room_type_id = ?", roomType.Id).Error; err == nil {
		for _, pic := range pictures {
			log.Println(pic)
			picturesView = append(picturesView, &roompb.PictureRoomData{
				RoomTypeId:           pic.RoomTypeId,
				Filename:             pic.File,
				AltText:              func () string {
					if pic.AltText != nil {
						return *pic.AltText
					}

					return ""
				}() ,
				Main:                 pic.Main,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			})
		}
	}

	return &roompb.GetRoomTypeResponse{
		RoomType:             roomTypeToGrpc(roomType),
		Pictures: 			  picturesView,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) FindRoomTypes(ctx context.Context, req *roompb.FindRoomTypesRequest) (*roompb.FindRoomTypesResponse, error) {
	filter := req.GetFilter()
	sort := req.GetSort()
	hotelId := req.GetHotelId()
	
	if sort == "" {
		sort = "name"
	}
	
	direction := req.GetSortDirection()
	
	if direction == "" {
		direction = "asc"
	}
	
	roomTypes := []model.RoomType{}
	
	if err := r.db.Where("hotel_id = ? AND name like ?", hotelId, "%" + filter + "%").Order("id ASC").Find(&roomTypes).Order([]string{sort + " " + direction}).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	result := []*roompb.RoomTypeViewList{}
	
	for _, rtype := range roomTypes {
		result = append(result, &roompb.RoomTypeViewList{
			Id:                   rtype.Id,
			Name:                 rtype.Name,
			MainPicture: 		  func () string {
				picture := model.RoomPicture{}
				if err := r.db.First(&picture, "main = ? AND room_type_id = ?", true, rtype.Id).Error; err != nil  {
					return ""
				}

				return picture.File
			}(),
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})
	}
	
	return &roompb.FindRoomTypesResponse{
		RoomTypes:            result,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) UploadPictureRoom(stream roompb.RoomServices_UploadPictureRoomServer) error {
	req, err := stream.Recv()

	os.Mkdir("media", os.ModePerm)

	if err != nil {
		return status.Error(codes.Internal, fmt.Sprintf("No info reqtrieved %v", err))
	}

	filesize := 0
	filename := req.GetPictureData().GetFilename()
	roomTypeId := req.GetPictureData().GetRoomTypeId()
	fileData := bytes.Buffer{}
	main := req.GetPictureData().GetMain()
	altText := req.GetPictureData().GetAltText()

	splitedFilename := strings.Split(filename, ".")

	extension := strings.ToLower(splitedFilename[len(splitedFilename) - 1])

	if extension != "jpg" && extension != "jpeg" && extension != "png" {
		return status.Error(codes.InvalidArgument, fmt.Sprintf("Not valid format"))
	}

	newFilename := utils.RandomLowerString(12) + "." + extension
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			return status.Error(codes.Internal, fmt.Sprintf("Error uploading the file %v", err))
		}

		chunk := req.GetContent()
		size := len(chunk)

		filesize += size

		if float32(filesize) / (1024 * 1024) > r.maxUploadSize {
			log.Printf("Max upload size exceed %v", float32(filesize) / (1024 * 1024))
			return status.Error(codes.InvalidArgument, fmt.Sprintf("it has exceed the upload limit size of %vM!!!", r.maxUploadSize))
		}

		_, err2 := fileData.Write(chunk)

		if err2 != nil {
			log.Println(err2)
			return status.Error(codes.Internal, fmt.Sprintf("Error uploading the file %v", err))
		}
	}

	log.Println(filesize)

	if filesize > 0 {
		fileToSave, err := os.Create("media/" + newFilename)

		if err != nil {
			return status.Error(codes.Internal, fmt.Sprintf("Error creating file %v", err))
		}
		defer fileToSave.Close()

		fileData.WriteTo(fileToSave)

		roomType := model.RoomType{}

		picture := model.RoomPicture{}

		picture.RoomTypeId = roomTypeId
		picture.File = newFilename
		if altText != "" {
			picture.AltText = &altText
		}
		picture.Created = time.Now()
		picture.Modified = time.Now()

		r.db.Create(&picture)

		if main {
			//disabled all main pictures before
			pictures := []model.RoomPicture{}

			if err := r.db.Find(&pictures, "room_type_id = ?", roomTypeId).Error; err == nil {
				for _, pic := range pictures {
					pic.Main = false
					r.db.Save(&pic)
				}
			}

			picture.Main = true

		}

		picture.Created = time.Now()
		picture.Modified = time.Now()

		r.db.Create(&picture)

		if err := r.db.First(&roomType, roomTypeId).Preload("Pictures").Error; err != nil {
			return status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}

		//generating thumbnails for the picture
		go func() {
			for index, value := range r.resolutions {
				os.Mkdir("./media/" + index, os.ModePerm)
				utils.GenerateThumbnail(newFilename, value, index)
			}
		}()

		stream.SendAndClose(&roompb.UploadPictureRoomResponse{
			Filename:             newFilename,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})
	}else {
		stream.SendAndClose(&roompb.UploadPictureRoomResponse{
			Filename:             "",
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})
	}

	return nil
}

func (r *room) PutPictureMain(ctx context.Context, req *roompb.PutPictureMainRequest) (*roompb.PutPictureMainResponse, error) {
	id := req.GetPictureId()
	filename := req.GetFilename()

	picture := model.RoomPicture{}

	if id != 0 {
		if err := r.db.First(&picture, id).Error; err != nil {
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}	
	}else {
		if err := r.db.First(&picture, "file = ?", filename).Error; err != nil {
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}
	}
	
	pictures := []model.RoomPicture{}
	
	if err := r.db.Find(&pictures, "room_type_id = ?", picture.RoomTypeId).Error; err == nil {
		for _, pic := range pictures {
			pic.Main = false
			
			r.db.Save(&pic)
		}
	}

	picture.Main = true
	
	if err := r.db.Save(&picture).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &roompb.PutPictureMainResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}
func (r *room) DeletePicture(ctx context.Context, req *roompb.DeletePictureRequest) (*roompb.DeletePictureResponse, error) {
	id := req.GetPictureId()
	filename := req.GetFilename()

	picture := model.RoomPicture{}

	if id != 0 {
		if err := r.db.First(&picture, id).Error; err != nil {
			log.Println(err)
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}
	}else {
		if err := r.db.First(&picture, "file = ?", filename).Error; err != nil {
			log.Println(err)
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}
	}

	if err := r.db.Unscoped().Delete(&picture).Error; err != nil {
		log.Println("error")
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	os.Remove("media/" + picture.File)

	go func() {
		for index, _ := range r.resolutions {
			os.Remove("media/" + index + "/" + picture.File)
		}
	}()
	
	return &roompb.DeletePictureResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (r *room) GetPicture(req *roompb.GetPictureRequest, stream roompb.RoomServices_GetPictureServer) error {
	id  := req.GetId()
	filename := req.GetFilename()

	picture := model.RoomPicture{}

	if id !=0 {
		if err := r.db.First(&picture, id).Error; err != nil {
			return status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}
	}else {
		if err := r.db.First(&picture, "file = ?", filename).Error; err != nil {
			return status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}
	}

	file, err := os.Open("media/" + picture.File)

	if err != nil {
		return status.Error(codes.NotFound, fmt.Sprintf("Error reading file: %v", err))
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	buffer := make([]byte, 1024)

	for {
		n, err := reader.Read(buffer)
		log.Println(n)
		if err == io.EOF {
			break
		}
		if err != nil {
			return status.Error(codes.NotFound, fmt.Sprintf("Cannot read chunk to buffer: %v", err))
		}

		res := &roompb.GetPictureResponse{
			Data:                 &roompb.GetPictureResponse_Content{Content: buffer[:n]},
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		}

		err = stream.Send(res)
		if err != nil {
			return status.Error(codes.NotFound, fmt.Sprintf("Error sending content file: %v", err))
		}
	}

	res := &roompb.GetPictureResponse{
		Data:                 &roompb.GetPictureResponse_FileData{FileData: &roompb.FileData{
			Id:                   picture.Id,
			Filename:             picture.File,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		}},
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	err = stream.Send(res)

	if err != nil {
		return err
	}

	return nil
}

func (r *room) BookingsCalendar(ctx context.Context, req *roompb.BookingsCalendarRequest) (*roompb.BookingsCalendarResponse, error) {
	start, err := time.Parse("2006-01-02", req.GetStart())

	if err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error parsing start date: %v", err))
	}

	end, err := time.Parse("2006-01-02", req.GetEnd())

	if err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error parsing end date: %v", err))
	}

	hotelId := req.GetHotelId()

	rooms := []model.Room{}

	if err := r.db.Preload("Type").Order("floor ASC, name ASC").Find(&rooms, "hotel_id = ?", hotelId).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	//check all dates for each room
	states := []*roompb.RoomState{}
	for _, room := range rooms {
		currentDate := start

		cont := 0
		for {
			cont++
			if currentDate.Unix() >= end.Unix() {
				break
			}

			bookingDate := model.BookingDate{}


			if err := r.db.Order("room_id ASC").First(&bookingDate, "room_id = ? AND date = ?", room.Id, currentDate).Error; err == nil {

				booking := model.Booking{}

				r.db.Preload("Client").First(&booking, bookingDate.BookingId)

				if currentDate.Unix() == start.Unix() {
					states = append(states, &roompb.RoomState{
						Id:                   booking.Id,
						IdRoom:               room.Id,
						Title:                room.Name + " -> " + func () string {
							name := booking.Client.FirstName
							if booking.Client.LastName != nil {
								name =  name + " " + *booking.Client.LastName
							}
							if booking.Client.SecondLastName != nil {
								name =  name + " " + *booking.Client.SecondLastName
							}

							return name + " #N" + strconv.Itoa(int(booking.Number)) + "/" + booking.DateBegin.Format("2006")
						}(),
						Free:                 false,
						Start:                currentDate.Format("2006-01-02"),
						End:                  currentDate.Format("2006-01-02"),
						CheckIn:              func () string {
							if booking.CheckIn != nil {
								return booking.CheckIn.Format("2006-01-02")
							}

							return ""
						}(),
						XXX_NoUnkeyedLiteral: struct{}{},
						XXX_unrecognized:     nil,
						XXX_sizecache:        0,
					})

				}else {
					if states[len(states) - 1].Id == booking.Id {
						states[len(states) - 1].Free = false
						states[len(states) - 1].End = func () string {
							/*
							if cont == 7 {
								next := currentDate.Add(24 * time.Hour)
								return next.Format("2006-01-02")
							}
							*/
							next := currentDate.Add(24 * time.Hour)
							return next.Format("2006-01-02")
						}()
						states[len(states) - 1].CheckIn = func () string {
							if booking.CheckIn != nil {
								return booking.CheckIn.Format("2006-01-02")
							}

							return ""
						}()

						//lastDate := booking.DateEnd.Add(-24 * time.Hour)
						lastDate := booking.DateEnd
						if currentDate.Unix() == lastDate.Unix() && cont < 7 {
							states = append(states, &roompb.RoomState{
								Id:                   0,
								IdRoom:               room.Id,
								Title:                room.Name,
								Free:                 true,
								Start:                lastDate.Format("2006-01-02"),
								End:                  lastDate.Format("2006-01-02"),
								CheckIn: 			  "",
								XXX_NoUnkeyedLiteral: struct{}{},
								XXX_unrecognized:     nil,
								XXX_sizecache:        0,
							})
						}
					}else {
						states = append(states, &roompb.RoomState{
							Id:                   booking.Id,
							IdRoom:               booking.RoomId,
							Title:                room.Name + " -> " + func () string {
								name := booking.Client.FirstName
								if booking.Client.LastName != nil {
									name =  name + " " + *booking.Client.LastName
								}
								if booking.Client.SecondLastName != nil {
									name =  name + " " + *booking.Client.SecondLastName
								}

								return name + " #N" + strconv.Itoa(int(booking.Number)) + "/" + booking.DateBegin.Format("2006")
							}(),
							Free:                 false,
							Start:                currentDate.Format("2006-01-02"),
							End:                  currentDate.Format("2006-01-02"),
							CheckIn:              func () string {
								if booking.CheckIn != nil {
									return booking.CheckIn.Format("2006-01-02")
								}

								return ""
							}(),
							XXX_NoUnkeyedLiteral: struct{}{},
							XXX_unrecognized:     nil,
							XXX_sizecache:        0,
						})
					}
				}
			}else {
				states = append(states, &roompb.RoomState{
					Id:                   0,
					IdRoom:               room.Id,
					Title:                room.Name,
					Free:                 true,
					Start:                currentDate.Format("2006-01-02"),
					End:                  currentDate.Format("2006-01-02"),
					CheckIn: 			  "",
					XXX_NoUnkeyedLiteral: struct{}{},
					XXX_unrecognized:     nil,
					XXX_sizecache:        0,
				})
			}

			currentDate = currentDate.Add(24 * time.Hour)
		}
	}


	return &roompb.BookingsCalendarResponse{
		Rooms:                states,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func roomTypeToModel(rtype *roompb.RoomType) model.RoomType {
	return model.RoomType{
		Id:          rtype.GetId(),
		Name:        rtype.GetName(),
		Pictures:    nil,
		DisabledWeb: rtype.GetDisabledWeb(),
		BasePrice:   rtype.GetBasePrice(),
		HotelId: 	 func () *int32 {
			if rtype.GetHotelId() != 0 {
				hotelId := rtype.GetHotelId()
				return &hotelId
			}

			return nil
		}(),
	}
}

func roomTypeToGrpc(rtype model.RoomType) *roompb.RoomTypeSingleView {
	return &roompb.RoomTypeSingleView{
		Id:                   rtype.Id,
		Name:                 rtype.Name,
		DisabledWeb:          rtype.DisabledWeb,
		HotelId: 			  *rtype.HotelId,
		BasePrice: 			  rtype.BasePrice,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}