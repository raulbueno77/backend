package task

import (
	model "ceginfor/cehotel-server/models"
	taskpb "ceginfor/cehotel-server/proto/task"
	"context"
	"fmt"
	"github.com/biezhi/gorm-paginator/pagination"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

type task struct {
	srv			*grpc.Server
	db			*gorm.DB
	paginator	int32
}

func NewTaskServices(srv *grpc.Server, db *gorm.DB, paginator int32) *task {
	return &task{
		srv:       srv,
		db:        db,
		paginator: paginator,
	}
}

func (t *task) RegisterServices() {
	taskpb.RegisterTaskServicesServer(t.srv, t)
}

func (t *task) CreateTaskType(ctx context.Context, req *taskpb.CreateTaskTypeRequest) (*taskpb.CreateTaskTypeResponse, error) {
	item := req.GetTaskType()
	
	taskType := TaskTypeToModel(item)
	
	taskType.Created = time.Now()
	taskType.Modified = time.Now()
	
	if err := t.db.Create(&taskType).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &taskpb.CreateTaskTypeResponse{
		TaskType:             TaskTypeToGrpc(taskType),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (t *task) UpdateTaskType(ctx context.Context, req *taskpb.UpdateTaskTypeRequest) (*taskpb.UpdateTaskTypeResponse, error) {
	item := req.GetTaskType()

	taskTypeDb := model.TaskType{}
	
	if err := t.db.First(&taskTypeDb, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	taskType := TaskTypeToModel(item)

	taskType.Created = taskTypeDb.Created
	taskType.Modified = time.Now()

	if err := t.db.Save(&taskType).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &taskpb.UpdateTaskTypeResponse{
		TaskType:             TaskTypeToGrpc(taskType),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (t *task) DeleteTaskType(ctx context.Context, req *taskpb.DeleteTaskTypeRequest) (*taskpb.DeleteTaskTypeResponse, error) {
	id := req.GetTaskTypeId()

	taskType := model.TaskType{}

	if err := t.db.First(&taskType, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	if err := t.db.Unscoped().Delete(&taskType).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &taskpb.DeleteTaskTypeResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (t *task) GetTaskType(ctx context.Context, req *taskpb.GetTaskTypeRequest) (*taskpb.GetTaskTypeResponse, error) {
	id := req.GetTaskTypeId()

	taskType := model.TaskType{}

	if err := t.db.First(&taskType, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &taskpb.GetTaskTypeResponse{
		TaskType:             TaskTypeToGrpc(taskType),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (t *task) FindTaskTypes(ctx context.Context, req *taskpb.FindTaskTypesRequest) (*taskpb.FindTaskTypesResponse, error) {
	filter := req.GetFilter()
	
	taskTypes := []model.TaskType{}
	
	if err := t.db.Where("name like ?", "%" + filter + "%").Find(&taskTypes).Order("name asc").Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	result := []*taskpb.TaskTypeView{}
	
	for _, ttype := range taskTypes {
		result = append(result, TaskTypeToGrpc(ttype))
	}
	
	return &taskpb.FindTaskTypesResponse{
		TaskType:             result,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (t *task) CreateTask(ctx context.Context, req *taskpb.CreateTaskRequest) (*taskpb.CreateTaskResponse, error) {
	item := req.GetTask()

	task := TaskToModel(item)
	task.Created = time.Now()
	task.Modified = time.Now()

	if err := t.db.Create(&task).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &taskpb.CreateTaskResponse{
		Task:                 TaskToGrpc(task),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (t *task) UpdateTask(ctx context.Context, req *taskpb.UpdateTaskRequest) (*taskpb.UpdateTaskResponse, error) {
	item := req.GetTask()

	taskDb := model.Task{}

	if err := t.db.First(&taskDb, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	task := TaskToModel(item)
	task.Created = taskDb.Created
	task.Modified = time.Now()

	if err := t.db.Save(&task).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &taskpb.UpdateTaskResponse{
		Task:                 TaskToGrpc(task),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (t *task) DeleteTask(ctx context.Context, req *taskpb.DeleteTaskRequest) (*taskpb.DeleteTaskResponse, error) {
	id := req.GetTaskId()

	task := model.Task{}

	if err := t.db.First(&task, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	if err := t.db.Unscoped().Delete(&task).Error; err != nil  {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &taskpb.DeleteTaskResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (t *task) GetTask(ctx context.Context, req *taskpb.GetTaskRequest) (*taskpb.GetTaskResponse, error) {
	id := req.GetTaskId()

	task := model.Task{}

	if err := t.db.First(&task, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	return &taskpb.GetTaskResponse{
		Task:                 TaskToGrpc(task),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (t *task) FindTasks(ctx context.Context, req *taskpb.FindTasksRequest) (*taskpb.FindTasksResponse, error) {

	begin, err := time.Parse("2006-01-02 15:04:05", req.GetDateBegin())

	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Begining date error: %v", err))
	}

	end, err := time.Parse("2006-01-02 15:04:05", req.GetDateEnd())

	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Ending date error: %v", err))
	}

	userId := req.GetUserId()

	hotelId := req.GetHotelId()

	page := req.GetPage()

	if page == 0 {
		page = 1
	}

	sort := req.GetSort()

	if sort == "" {
		sort = "tasks.date"
	}

	direction := req.GetSortDirection()

	if direction == "" {
		direction = "ASC"
	}

	tasks := []model.Task{}

	query := &gorm.DB{}

	if userId != 0 {
		query = t.db.Table("tasks").Select("tasks.*").Joins("join task_types on tasks.task_type_id = task_types.id").Joins("join users on tasks.user_id = users.id").Joins("join rooms on tasks.room_id = rooms.id").Joins("join hotels on rooms.hotel_id = hotels.id").Preload("User").Preload("TaskType").Preload("Room").Preload("Room.Hotel").Where("rooms.hotel_id = ? AND tasks.user_id = ? AND tasks.date BETWEEN ? AND ?", hotelId, userId, begin, end)
	}else {
		query = t.db.Table("tasks").Select("tasks.*").Joins("join task_types on tasks.task_type_id = task_types.id").Joins("join users on tasks.user_id = users.id").Joins("join rooms on tasks.room_id = rooms.id").Joins("join hotels on rooms.hotel_id = hotels.id").Preload("User").Preload("TaskType").Preload("Room").Preload("Room.Hotel").Where("rooms.hotel_id = ? AND tasks.date BETWEEN ? AND ?", hotelId, begin, end)
	}

	pag := pagination.Paging(&pagination.Param{
		DB:      query,
		Page:    int(page),
		Limit:   int(t.paginator),
		OrderBy: []string{sort + " " + direction},
		ShowSQL: true,
	}, &tasks)


	result := []*taskpb.TaskView{}

	for _, tk := range tasks {
		result = append(result, TaskToGrpc(tk))
	}

	return &taskpb.FindTasksResponse{
		Task:                 result,
		TotalPages:           int32(pag.TotalPage),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func TaskTypeToModel(ttype *taskpb.TaskType) model.TaskType {
	return model.TaskType{
		Id:       ttype.GetId(),
		Name:     ttype.GetName(),
	}
}

func TaskTypeToGrpc(ttype model.TaskType) *taskpb.TaskTypeView {
	return &taskpb.TaskTypeView{
		Id:                   ttype.Id,
		Name:                 ttype.Name,
		Created:              ttype.Created.Format("2006-01-02 15:04:05"),
		Modified:             ttype.Modified.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

func TaskToModel(task *taskpb.Task) model.Task {
	return model.Task{
		Id:          task.GetId(),
		Date: 		 func () time.Time {
			date, err := time.Parse("2006-01-02 15:04:05", task.Date)
			if err != nil {
				return time.Time{}
			}

			return date
		}(),
		RoomId:      func () *int32 {
			if task.GetRoomId() == 0 {
				return nil
			}
			room_id := task.GetRoomId()
			return &room_id
		}(),
		UserId:      task.GetUserId(),
		TaskTypeId:  task.GetTaskTypeId(),
		Description: func () *string {
			if task.GetDescription() == "" {
				return nil
			}
			description := task.GetDescription()

			return &description
		}(),
	}
}

func TaskToGrpc(task model.Task) *taskpb.TaskView {
	return &taskpb.TaskView{
		Id:                   task.Id,
		Date:				  task.Date.Format("2006-01-02 15:04:05"),
		Room:                 func () *taskpb.TaskEntities {

			if task.Room == nil {
				return nil
			}

			return &taskpb.TaskEntities{
				Id:                   task.Room.Id,
				Name:                 task.Room.Name,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		User:                 func () *taskpb.TaskEntities {
			return &taskpb.TaskEntities{
				Id:                   task.User.Id,
				Name:                 task.User.Firstname + " " + task.User.Lastname,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		TaskType:              func () *taskpb.TaskEntities {
			return &taskpb.TaskEntities{
				Id:                   task.TaskType.Id,
				Name:                 task.TaskType.Name,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		Description:          func () string {
			if task.Description != nil {
				return *task.Description
			}
			
			return ""
		}(),
		Created:              task.Created.Format("2006-01-02 15:04:05"),
		Modified:             task.Modified.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}