package saleInvoice

import (
	model "ceginfor/cehotel-server/models"
	saleinvoicepb "ceginfor/cehotel-server/proto/sale_invoice"
	"ceginfor/cehotel-server/utils"
	"context"
	"fmt"
	"github.com/biezhi/gorm-paginator/pagination"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"strconv"
	"strings"
	"time"
)

type saleInvoice struct {
	srv			*grpc.Server
	db			*gorm.DB
	paginator	int32
}

func NewSaleInvoiceService(srv *grpc.Server, db *gorm.DB, paginator int32) *saleInvoice {
	return &saleInvoice{
		srv: srv,
		db:  db,
		paginator: paginator,
	}
}

func (s *saleInvoice) RegisterServices() {
	saleinvoicepb.RegisterSaleInvoiceServicesServer(s.srv, s)
}


func (s *saleInvoice) CreateSaleInvoice(ctx context.Context, req *saleinvoicepb.CreateSaleInvoiceRequest) (*saleinvoicepb.CreateSaleInvoiceResponse, error) {
	invoice := req.GetInvoice()
	details := req.GetDetails()

	client := model.Client{}

	//check if exists the client and created one or updated data
	if invoice.ClientId != 0 {
		if err := s.db.First(&client, invoice.ClientId).Error; err != nil {
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}
	}

	client.FirstName = invoice.FirstName
	client.LastName = func () *string {
		if invoice.LastName == "" {
			return nil
		}

		return &invoice.LastName
	}()
	log.Printf("El apellido es %v", *client.SecondLastName)
	client.SecondLastName = func () *string {
		if invoice.SecondLastName == "" {
			return nil
		}

		return &invoice.SecondLastName
	}()
	client.Address = func () *string {
		if invoice.Address == "" {
			return nil
		}

		return &invoice.Address
	}()
	client.City = func () *string {
		if invoice.City == "" {
			return nil
		}

		return &invoice.City
	}()
	client.Country = func () *string {
		if invoice.Country == "" {
			return nil
		}

		return &invoice.Country
	}()
	client.Region = func () *string {
		if invoice.Region == "" {
			return nil
		}

		return &invoice.Region
	}()
	client.PostalCode = func () *string {
		if invoice.PostalCode == "" {
			return nil
		}

		return &invoice.PostalCode
	}()
	client.CellPhone = func () *string {
		if invoice.CellPhone == "" {
			return nil
		}

		return &invoice.CellPhone
	}()
	client.HomePhone = func () *string {
		if invoice.HomePhone == "" {
			return nil
		}

		return &invoice.HomePhone
	}()
	client.Email = func () *string {
		if invoice.Email == "" {
			return nil
		}

		return &invoice.Email
	}()
	client.FullName = invoice.FirstName + " " + invoice.LastName + " " + invoice.SecondLastName

	if invoice.ClientId != 0 {
		if err := s.db.Save(&client).Error; err != nil {
			return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
		}
	}else {
		if err := s.db.Create(&client).Error; err != nil {
			return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
		}
	}

	//save the invoice
	invoiceDb := s.toModelInvoice(invoice)

	invoiceDb.Created = time.Now()
	invoiceDb.Modified = time.Now()

	if err := s.db.Debug().Create(&invoiceDb).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	detailsReturn := []*saleinvoicepb.SaleInvoiceDetail{}

	for _, det := range details {
		detailDb := s.toModelDetail(det)
		detailDb.Id = 0
		detailDb.SaleInvoiceId = invoiceDb.Id

		if err := s.db.Debug().Create(&detailDb).Error; err != nil {
			return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
		}

		detailsReturn = append(detailsReturn, s.toGrpcDetail(detailDb))
	}

	return &saleinvoicepb.CreateSaleInvoiceResponse{
		Invoice:              s.toGrpcInvoice(invoiceDb),
		Details:              detailsReturn,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (s *saleInvoice) UpdateSaleInvoice(ctx context.Context, req *saleinvoicepb.UpdateSaleInvoiceRequest) (*saleinvoicepb.UpdateSaleInvoiceResponse, error) {
	invoice := req.GetInvoice()
	details := req.GetDetails()

	client := model.Client{}

	//check if exists the client and created one or updated data
	if invoice.ClientId != 0 {
		if err := s.db.First(&client, invoice.ClientId).Error; err != nil {
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}
	}

	client.FirstName = invoice.FirstName
	client.LastName = func () *string {
		if invoice.LastName == "" {
			return nil
		}

		return &invoice.LastName
	}()
	client.SecondLastName = func () *string {
		if invoice.SecondLastName == "" {
			return nil
		}

		return &invoice.SecondLastName
	}()
	client.Address = func () *string {
		if invoice.Address == "" {
			return nil
		}

		return &invoice.Address
	}()
	client.City = func () *string {
		if invoice.City == "" {
			return nil
		}

		return &invoice.City
	}()
	client.Country = func () *string {
		if invoice.Country == "" {
			return nil
		}

		return &invoice.Country
	}()
	client.Region = func () *string {
		if invoice.Region == "" {
			return nil
		}

		return &invoice.Region
	}()
	client.PostalCode = func () *string {
		if invoice.PostalCode == "" {
			return nil
		}

		return &invoice.PostalCode
	}()
	client.CellPhone = func () *string {
		if invoice.CellPhone == "" {
			return nil
		}

		return &invoice.CellPhone
	}()
	client.HomePhone = func () *string {
		if invoice.HomePhone == "" {
			return nil
		}

		return &invoice.HomePhone
	}()
	client.Email = func () *string {
		if invoice.Email == "" {
			return nil
		}

		return &invoice.Email
	}()
	client.FullName = invoice.FirstName + " " + invoice.LastName + " " + invoice.SecondLastName

	if invoice.ClientId != 0 {
		if err := s.db.Save(&client).Error; err != nil {
			return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
		}
	}else {
		if err := s.db.Create(&client).Error; err != nil {
			return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
		}
	}

	invoiceBefore := model.SaleInvoice{}

	if err := s.db.First(&invoiceBefore, invoice.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	invoiceDb := s.toModelInvoice(invoice)

	invoiceDb.Created = invoiceBefore.Created
	invoiceDb.Modified = time.Now()

	if err := s.db.Save(&invoiceDb).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	detailsReturn := []*saleinvoicepb.SaleInvoiceDetail{}


	s.db.Debug().Exec("DELETE FROM sale_invoice_details WHERE sale_invoice_id = ?", invoice.GetId())

	for _, det := range details {
		detailDb := s.toModelDetail(det)
		detailDb.Id = 0
		detailDb.SaleInvoiceId = invoice.GetId()

		if err := s.db.Debug().Create(&detailDb).Error; err != nil {
			return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
		}

		detailsReturn = append(detailsReturn, s.toGrpcDetail(detailDb))
	}

	return &saleinvoicepb.UpdateSaleInvoiceResponse{
		Invoice:              s.toGrpcInvoice(invoiceDb),
		Details:              detailsReturn,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (s *saleInvoice) DeleteSaleInvoice(ctx context.Context, req *saleinvoicepb.DeleteSaleInvoiceRequest) (*saleinvoicepb.DeleteSaleInvoiceResponse, error) {
	id := req.GetId()

	invoiceDb := model.SaleInvoice{}

	if err := s.db.First(&invoiceDb, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	if err := s.db.Unscoped().Delete(&invoiceDb).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &saleinvoicepb.DeleteSaleInvoiceResponse{
		Message:              "Operation ok!!",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (s *saleInvoice) GetSaleInvoice(ctx context.Context, req *saleinvoicepb.GetSaleInvoiceRequest) (*saleinvoicepb.GetSaleInvoiceResponse, error) {
	id := req.GetId()

	invoiceDb := model.SaleInvoice{}

	if err := s.db.First(&invoiceDb, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	details := []model.SaleInvoiceDetail{}

	s.db.Find(&details, "sale_invoice_id = ?", id)

	detailsReturn := []*saleinvoicepb.SaleInvoiceDetail{}

	for _, det := range details {
		detailsReturn = append(detailsReturn, s.toGrpcDetail(det))
	}

	return &saleinvoicepb.GetSaleInvoiceResponse{
		Invoice:              s.toGrpcInvoice(invoiceDb),
		Details:              detailsReturn,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (s *saleInvoice) FindSaleInvoices(ctx context.Context, req *saleinvoicepb.FindSaleInvoicesRequest) (*saleinvoicepb.FindSaleInvoicesResponse, error) {
	clientId := req.GetClientId()
	begin, err := time.Parse("2006-01-02", req.GetBegin())
	if err != nil {
		begin, _ = time.Parse("2006-01-02", time.Now().Format("2006") + "-01-01")

	}

	end, err := time.Parse("2006-01-02", req.GetEnd())
	if err != nil {
		end, _ = time.Parse("2006-01-02 15:04:05", time.Now().Format("2006") + "-12-31 23:59:59")

	}
	page := req.GetPage()
	if page == 0 {
		page = 1
	}
	sort := req.GetSort()
	if sort == "" {
		sort = "id"
	}
	direction := req.GetSortDirection()
	if direction == "" {
		direction =  "ASC"
	}
	filter := "%" + strings.TrimSpace(req.GetFilter()) + "%"

	invoices := []model.SaleInvoice{}

	db := &gorm.DB{}

	serialId := req.GetSerialId()

	if clientId == 0 {
		if serialId == 0 {
			db = s.db.Where("date BETWEEN ? AND ? AND (first_name like ? OR last_name like ? OR second_last_name like ? OR document like ? OR city like ? OR region like ? OR country like ?)", begin, end, filter, filter, filter, filter, filter, filter, filter)
		}else {
			db = s.db.Where("date BETWEEN ? AND ? AND (first_name like ? OR last_name like ? OR second_last_name like ? OR document like ? OR city like ? OR region like ? OR country like ?) AND serial_id = ?", begin, end, filter, filter, filter, filter, filter, filter, filter, serialId)
		}
	}else {
		if serialId == 0 {
			db = s.db.Where("client_id = ? AND date BETWEEN ? AND ? AND (first_name like ? OR last_name like ? OR second_last_name like ? OR document like ? OR city like ? OR region like ? OR country like ?)", clientId, begin, end, filter, filter, filter, filter, filter, filter, filter)
		}else {
			db = s.db.Where("client_id = ? AND date BETWEEN ? AND ? AND (first_name like ? OR last_name like ? OR second_last_name like ? OR document like ? OR city like ? OR region like ? OR country like ?) AND serial_id = ?", clientId, begin, end, filter, filter, filter, filter, filter, filter, filter, serialId)
		}
	}

	pag := pagination.Paging(&pagination.Param{
		DB:      db,
		Page:    int(page),
		Limit:   int(s.paginator),
		OrderBy: []string{sort + " " + direction},
		ShowSQL: true,
	}, &invoices)

	invoicesReturn := []*saleinvoicepb.SaleInvoice{}

	for _, inv := range invoices {
		invoicesReturn = append(invoicesReturn, s.toGrpcInvoice(inv))
	}

	return &saleinvoicepb.FindSaleInvoicesResponse{
		Invoices:             invoicesReturn,
		TotalPages:           int32(pag.TotalPage),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (s *saleInvoice) CreateSerialInvoice(ctx context.Context, req *saleinvoicepb.CreateSerialInvoiceRequest) (*saleinvoicepb.CreateSerialInvoiceResponse, error) {
	item := req.GetSerial()

	serial := s.toModelSerial(item)

	if err := s.db.Create(&serial).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &saleinvoicepb.CreateSerialInvoiceResponse{
		Serial:               s.toGrpcSerial(serial),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (s *saleInvoice) UpdateSerialInvoice(ctx context.Context, req *saleinvoicepb.UpdateSerialInvoiceRequest) (*saleinvoicepb.UpdateSerialInvoiceResponse, error) {
	item := req.GetSerial()

	serial := s.toModelSerial(item)

	if err := s.db.Save(&serial).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &saleinvoicepb.UpdateSerialInvoiceResponse{
		Serial:               s.toGrpcSerial(serial),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (s *saleInvoice) DeleteSerialInvoice(ctx context.Context, req *saleinvoicepb.DeleteSerialInvoiceRequest) (*saleinvoicepb.DeleteSerialInvoiceResponse, error) {
	id := req.GetId()

	serial := model.SerialInvoice{}

	if err := s.db.First(&serial, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	if err := s.db.Unscoped().Delete(&serial).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &saleinvoicepb.DeleteSerialInvoiceResponse{
		Message:              "Operation ok!!",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (s *saleInvoice) GetSerialInvoice(ctx context.Context, req *saleinvoicepb.GetSerialInvoiceRequest) (*saleinvoicepb.GetSerialInvoiceResponse, error) {
	id := req.GetId()

	serial := model.SerialInvoice{}

	if err := s.db.First(&serial, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	return &saleinvoicepb.GetSerialInvoiceResponse{
		Serial:               s.toGrpcSerial(serial),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (s *saleInvoice) FindSerialInvoices(ctx context.Context, req *saleinvoicepb.FindSerialInvoicesRequest) (*saleinvoicepb.FindSerialInvoicesResponse, error) {

	serialsDb := []model.SerialInvoice{}

	if err := s.db.Find(&serialsDb).Order("id ASC").Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	serials := []*saleinvoicepb.SerialInvoice{}

	for _, serial := range serialsDb {
		serials = append(serials, s.toGrpcSerial(serial))
	}

	return &saleinvoicepb.FindSerialInvoicesResponse{
		Serial:               serials,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (s *saleInvoice) NextNumber(ctx context.Context, req *saleinvoicepb.NextNumberRequest) (*saleinvoicepb.NextNumberResponse, error) {

	serialId := req.GetSerial()
	year := req.GetYear()

	if year == 0 {
		aux, _ := strconv.Atoi(time.Now().Format("Y"))
		year = int32(aux)
	}

	number := utils.GetNextNumberInvoice(serialId, year, s.db)

	return &saleinvoicepb.NextNumberResponse{
		Number:               number,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (s *saleInvoice) toModelInvoice (inv *saleinvoicepb.SaleInvoice) model.SaleInvoice {
	return model.SaleInvoice{
		Id:              inv.GetId(),
		ClientId:        inv.GetClientId(),
		FirstName:       inv.GetFirstName(),
		LastName:        func () *string {
			if inv.GetLastName() == "" {
				return nil
			}

			toReturn := inv.GetLastName()

			return &toReturn
		}(),
		SecondLastName:  func () *string {
			if inv.GetSecondLastName() == "" {
				return nil
			}

			toReturn := inv.GetSecondLastName()

			return &toReturn
		}(),
		Document:        inv.GetDocument(),
		Address:         func () *string {
			if inv.GetAddress() == "" {
				return nil
			}

			toReturn := inv.GetAddress()

			return &toReturn
		}(),
		PostalCode:      func () *string {
			if inv.GetPostalCode() == "" {
				return nil
			}

			toReturn := inv.GetPostalCode()

			return &toReturn
		}(),
		Country:         func () *string {
			if inv.GetCountry() == "" {
				return nil
			}

			toReturn := inv.GetCountry()

			return &toReturn
		}(),
		Region:          func () *string {
			if inv.GetRegion() == "" {
				return nil
			}

			toReturn := inv.GetRegion()

			return &toReturn
		}(),
		City:            func () *string {
			if inv.GetCity() == "" {
				return nil
			}

			toReturn := inv.GetCity()

			return &toReturn
		}(),
		HomePhone:       func () *string {
			if inv.GetHomePhone() == "" {
				return nil
			}

			toReturn := inv.GetHomePhone()

			return &toReturn
		}(),
		CellPhone:       func () *string {
			if inv.GetCellPhone() == "" {
				return nil
			}

			toReturn := inv.GetCellPhone()

			return &toReturn
		}(),
		Email:           func () *string {
			if inv.GetEmail() == "" {
				return nil
			}

			toReturn := inv.GetEmail()

			return &toReturn
		}(),
		Observations:    func () *string {
			if inv.GetObservations() == "" {
				return nil
			}

			toReturn := inv.GetObservations()

			return &toReturn
		}(),
		Date:            func () time.Time {
			date, err := time.Parse("2006-01-02", inv.GetDate())

			if err != nil {
				return time.Time{}
			}

			return date
		}(),
		Total:           inv.GetTotal(),
		Number:          func (num int32) int32 {

			if num != 0 {
				return num
			}

			//search last number for invoice
			type numberStruct struct {
				Number	int32 `json:"number"`
			}

			numberObj := numberStruct{}

			query := "SELECT MAX(number) as number FROM sale_invoices WHERE date BETWEEN ? AND ?"

			date, _ := time.Parse("2006-01-02", inv.GetDate())

			begin, _ := time.Parse("2006-01-02", date.Format("2006") + "-01-01")
			end, _ := time.Parse("2006-01-02", date.Format("2006") + "-12-31")

			if inv.GetSerialInvoiceId() != 0 {
				query += " AND serial_invoice_id = ?"

				s.db.Debug().Raw(query, begin, end, inv.GetSerialInvoiceId()).Scan(&numberObj)
			}else {
				s.db.Raw(query, begin, end, 0).Scan(&numberObj)
			}

			return numberObj.Number + 1

		}(inv.GetNumber()),
		SerialInvoiceId: func () *int32 {
			if inv.GetSerialInvoiceId() == 0 {
				return nil
			}

			toReturn := inv.GetSerialInvoiceId()

			return &toReturn
		}(),
	}
}

func (s *saleInvoice) toGrpcInvoice (inv model.SaleInvoice) *saleinvoicepb.SaleInvoice {
	return &saleinvoicepb.SaleInvoice{
		Id:                   inv.Id,
		ClientId:             inv.ClientId,
		Document:             inv.Document,
		FirstName:            inv.FirstName,
		LastName:             func () string {
			if inv.LastName == nil {
				return ""
			}

			return *inv.LastName
		}(),
		SecondLastName:       func () string {
			if inv.SecondLastName == nil {
				return ""
			}

			return *inv.SecondLastName
		}(),
		Address:              func () string {
			if inv.Address == nil {
				return ""
			}

			return *inv.Address
		}(),
		PostalCode:           func () string {
			if inv.PostalCode == nil {
				return ""
			}

			return *inv.PostalCode
		}(),
		Country:              func () string {
			if inv.Country == nil {
				return ""
			}

			return *inv.Country
		}(),
		Region:               func () string {
			if inv.Region == nil {
				return ""
			}

			return *inv.Region
		}(),
		City:                 func () string {
			if inv.City == nil {
				return ""
			}

			return *inv.City
		}(),
		HomePhone:            func () string {
			if inv.HomePhone == nil {
				return ""
			}

			return *inv.HomePhone
		}(),
		CellPhone:            func () string {
			if inv.CellPhone == nil {
				return ""
			}

			return *inv.CellPhone
		}(),
		Email:                func () string {
			if inv.Email == nil {
				return ""
			}

			return *inv.Email
		}(),
		Observations:         func () string {
			if inv.Observations == nil {
				return ""
			}

			return *inv.Observations
		}(),
		Date:                 inv.Date.Format("2006-01-02"),
		Total:                inv.Total,
		SerialInvoiceId:      func () int32 {
			if inv.SerialInvoiceId != nil {
				return *inv.SerialInvoiceId
			}

			return 0
		}(),
		Number:               inv.Number,
		Created:              inv.Created.Format("2006-01-02 15:04:05"),
		Modified:             inv.Modified.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

func (s *saleInvoice) toModelDetail (det *saleinvoicepb.SaleInvoiceDetail) model.SaleInvoiceDetail {
	return model.SaleInvoiceDetail{
		Id:            det.GetId(),
		SaleInvoiceId: det.GetSaleInvoiceId(),
		Reference:     func () *string {
			if det.GetReference() == "" {
				return nil
			}

			toReturn := det.GetReference()

			return &toReturn
		}(),
		Description:   det.GetDescription(),
		Units:         det.GetUnits(),
		Tax:           det.GetTax(),
		Price:         det.GetPrice(),
		Total:         det.GetTotal(),
	}
}

func (s *saleInvoice) toGrpcDetail (det model.SaleInvoiceDetail) *saleinvoicepb.SaleInvoiceDetail {
	return &saleinvoicepb.SaleInvoiceDetail{
		Id:                   det.Id,
		SaleInvoiceId:        det.SaleInvoiceId,
		Reference:            func () string {
			if det.Reference == nil {
				return ""
			}

			return *det.Reference
		}(),
		Description:          det.Description,
		Units:                det.Units,
		Tax:                  det.Tax,
		Price:                det.Price,
		Total:                det.Total,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

func (s *saleInvoice) toModelSerial (ser *saleinvoicepb.SerialInvoice) model.SerialInvoice {
	return model.SerialInvoice{
		Id:     ser.GetId(),
		Serial: ser.GetSerial(),
		Record: ser.GetRecord(),
		Name: 	ser.GetName(),
	}
}

func (s *saleInvoice) toGrpcSerial (ser model.SerialInvoice) *saleinvoicepb.SerialInvoice {
	return &saleinvoicepb.SerialInvoice{
		Id:                   ser.Id,
		Serial:               ser.Serial,
		Record:               ser.Record,
		Name: 				  ser.Name,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}