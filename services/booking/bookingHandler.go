package booking

import (
	model "ceginfor/cehotel-server/models"
	bookingpb "ceginfor/cehotel-server/proto/booking"
	"ceginfor/cehotel-server/utils"
	"context"
	"fmt"
	"github.com/biezhi/gorm-paginator/pagination"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"strconv"
	"time"
)

type booking struct {
	srv			*grpc.Server
	db			*gorm.DB
	paginator	int32
}

func NewBookingService(srv *grpc.Server, db *gorm.DB, paginator int32) *booking {
	return &booking{
		srv:      srv,
		db:        db,
		paginator: paginator,
	}
}

func (b *booking) RegisterService() {
	bookingpb.RegisterBookingServicesServer(b.srv, b)
}

func (b *booking) CreateBooking(ctx context.Context, req *bookingpb.CreateBookingRequest) (*bookingpb.CreateBookingResponse, error) {
	item := req.GetBooking()

	begin, err := time.Parse("2006-01-02", item.GetDateBegin())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Begining date error: %v", err))
	}

	end, err := time.Parse("2006-01-02", item.GetDateEnd())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Ending date error: %v", err))
	}

	booking := toModel(item)
	booking.Created = time.Now()
	booking.Modified = time.Now()
	booking.DateBegin = begin
	booking.DateEnd = end

	bookDb := model.Booking{}


	bookingDates := []model.BookingDate{}

	if err := b.db.Debug().Find(&bookingDates, "(date BETWEEN ? AND ?) AND room_id = ?", begin, end.Add(-1 * time.Second), booking.RoomId).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}


	if len(bookingDates) > 0 {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Error dates"))
	}

		number := int32(0)

	if err := b.db.Order("number desc").First(&bookDb, "date_begin BETWEEN ? AND ?", begin.Format("2006") + "-01-01", begin.Format("2006") + "-12-31").Error; err != nil {
		number = int32(1)
	}else {
		number = bookDb.Number + 1
	}

	booking.Number = number

	if err := b.db.Create(&booking).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	diff := end.Sub(begin)

	counter := diff.Hours()

	for counter > 0 {
		date := model.BookingDate{
			Date:      begin,
			BookingId: booking.Id,
			RoomId:    booking.RoomId,
			Created:   time.Time{},
			Modified:  time.Time{},
		}

		b.db.Create(&date)

		begin = begin.Add(time.Hour * 24)
		counter -= 24
	}
	bookingDb := model.Booking{}
	
	if err := b.db.Preload("Room").Preload("Room.Type").Preload("Client").Preload("Dates").First(&bookingDb, booking.Id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	return &bookingpb.CreateBookingResponse{
		Booking:              toGrpc(bookingDb),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) UpdateBooking(ctx context.Context, req *bookingpb.UpdateBookingRequest) (*bookingpb.UpdateBookingResponse, error) {
	log.Println("Update booking...")
	item := req.GetBooking()

	bookingDb := model.Booking{}

	if err := b.db.Debug().First(&bookingDb, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error no booking: %v", err))
	}

	booking := toModel(item)
	booking.Number = bookingDb.Number

	begin, err := time.Parse("2006-01-02", item.GetDateBegin())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Begining date error: %v", err))
	}

	end, err := time.Parse("2006-01-02", item.GetDateEnd())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Ending date error: %v", err))
	}

	diff := end.Sub(begin)

	counter := diff.Hours()

	aux := begin


	/*
	for counter > 0 {
		ok := b.checkDate(aux, item.GetId(), booking.RoomId, "")

		if !ok {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprint("One or more dates are in use yet"))
		}


	}
	*/
	booking.DateBegin = begin
	booking.DateEnd = end
	booking.CheckIn = bookingDb.CheckIn

	counter = diff.Hours()

	b.db.Exec("DELETE FROM booking_dates WHERE booking_id = ?", item.GetId())

	for counter > 0 {
		bookingDate := model.BookingDate{}

		if err := b.db.Where("booking_id = ? AND date = ?", item.GetId(), aux).Find(&bookingDate).Error; err != nil {
			bookingDate = model.BookingDate{
				Date:      aux,
				BookingId: item.GetId(),
				RoomId:    booking.RoomId,
				Created:   time.Now(),
				Modified:  time.Now(),
			}

			b.db.Create(&bookingDate)
		}else {
			bookingDate.Modified = time.Now()
			b.db.Save(&bookingDate)
		}

		aux = aux.Add(time.Hour * 24)
		counter -= 24
	}

	booking.Created = bookingDb.Created
	booking.Modified = time.Now()

	if err := b.db.Save(&booking).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	if err := b.db.Preload("Room").Preload("Room.Type").Preload("Client").Preload("Dates").First(&bookingDb, booking.Id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	return &bookingpb.UpdateBookingResponse{
		Booking:              toGrpc(bookingDb),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) DeleteBooking(ctx context.Context, req *bookingpb.DeleteBookingRequest) (*bookingpb.DeleteBookingResponse, error) {
	id := req.GetId()

	booking := model.Booking{}

	if err := b.db.First(&booking, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	//delete guests
	guests := []model.Guest{}

	if err := b.db.Find(&guests, "booking_id = ?", booking.Id).Error; err == nil {
		for _, guest := range guests {
			b.db.Unscoped().Delete(&guest)
		}
	}

	if err := b.db.Unscoped().Delete(&booking).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &bookingpb.DeleteBookingResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) ManageCheckin(ctx context.Context, req *bookingpb.ManageCheckinRequest) (*bookingpb.ManageCheckinResponse, error) {
	checking := req.GetCheckin()

	booking := model.Booking{}

	if err := b.db.First(&booking, checking.GetBookingId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	now := time.Now()

	if checking.GetIn() {
		booking.CheckIn = &now
		guests := []model.Guest{}

		b.db.Find(&guests, "booking_id = ?", booking.Id)

		for _, guest := range guests {
			guest.CheckIn = now
			b.db.Save(&guest)
		}
	}else {
		booking.CheckIn = nil
		guests := []model.Guest{}

		b.db.Find(&guests, "booking_id = ?", booking.Id)

		for _, guest := range guests {
			guest.CheckIn = booking.DateBegin
			b.db.Save(&guest)
		}
	}

	booking.Modified = now

	if err := b.db.Save(&booking).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &bookingpb.ManageCheckinResponse{
		Checkin:             &bookingpb.CheckinView{
			BookingId:            booking.Id,
			In:                   checking.In,
			Date:                 now.Format("2006-01-02 15:04:05"),
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		},
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) GetBooking(ctx context.Context, req *bookingpb.GetBookingRequest) (*bookingpb.GetBookingResponse, error) {
	id := req.GetId()

	booking := model.Booking{}

	if id == 0 {
		begin, err := time.Parse("2006-01-02", req.GetBegin())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Begining date error: %v", err))
		}

		roomName := req.GetRoomName()
		hotelId := req.GetHotelId()

		room := model.Room{}

		if err := b.db.First(&room, "name = ? AND hotel_id = ?", roomName, hotelId).Error; err != nil {
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}

		bookingDate := model.BookingDate{}

		if err := b.db.First(&bookingDate, "date = ? AND room_id = ?", begin, room.Id).Error; err != nil {
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}

		id = bookingDate.BookingId

	}

	if err := b.db.Preload("Room").Preload("Room.Type").Preload("Client").Preload("Dates").First(&booking, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	return &bookingpb.GetBookingResponse{
		Booking:              toGrpc(booking),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) FindBookings(ctx context.Context, req *bookingpb.FindBookingsRequest) (*bookingpb.FindBookingsResponse, error) {
	begin, err := time.Parse("2006-01-02", req.GetDateBegin())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Begining date error: %v", err))
	}

	end, err := time.Parse("2006-01-02", req.GetDateEnd())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Ending date error: %v", err))
	}

	filter := req.GetFilter()

	page := req.GetPage()

	if page == 0 {
		page = 1
	}

	sort := req.GetSort()

	if sort == "" {
		sort = "bookings.date_begin"
	}

	direction := req.GetSortDirection()

	if direction == "" {
		direction = "desc"
	}

	where := "bookings.date_begin BETWEEN ? AND ? AND (clients.full_name like ? OR rooms.name like ? OR room_types.name like ?)"

	bookings := []model.Booking{}

	objDb := b.db.Table("bookings").Select("bookings.*").Joins("join clients on bookings.client_id = clients.id").Joins("join rooms on bookings.room_id = rooms.id").Joins("join room_types on rooms.type_id = room_types.id").Preload("Client").Preload("Room").Preload("Room.Type").Where(where, begin, end, "%" + filter + "%", "%" + filter + "%", "%" + filter + "%")

	pag := pagination.Paging(&pagination.Param{
		DB:      objDb,
		Page:    int(page),
		Limit:   int(b.paginator),
		OrderBy: []string{sort + " " + direction},
		ShowSQL: true,
	}, &bookings)

	result := []*bookingpb.BookingViewList{}

	for _, book := range bookings {
		result = append(result, toGrpcList(book))
	}

	return &bookingpb.FindBookingsResponse{
		Bookings:             result,
		TotalPages:           int32(pag.TotalPage),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) GetPrice(ctx context.Context, req *bookingpb.GetPriceRequest) (*bookingpb.GetPriceResponse, error) {
	begin, err := time.Parse("2006-01-02", req.GetDateBegin())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Begining date error: %v", err))
	}

	end, err := time.Parse("2006-01-02", req.GetDateEnd())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Ending date error: %v", err))
	}
	
	roomId := req.GetRoomId()

	room := model.Room{}

	if roomId != 0 {
		if err := b.db.Preload("Type").First(&room, roomId).Error; err != nil {
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}
	}else {
		roomName := req.GetRoomName()

		if err := b.db.Debug().Preload("Type").First(&room, "name = ?", roomName).Error; err != nil {
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}
	}

	diff := end.Sub(begin)

	counter := diff.Hours()

	price := float32(0.0)
	
	for counter > 0 {
		roomPrice := model.RoomPrice{}

		if err := b.db.Where("room_type_id = ? AND date = ?", room.TypeId, begin).Find(&roomPrice).Error; err != nil {
			price += room.Type.BasePrice
		}else {
			price += roomPrice.Price
		}
		
		begin = begin.Add(time.Hour * 24)
		counter -= 24
	}
	
	return &bookingpb.GetPriceResponse{
		Price:                price,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) CheckDates(ctx context.Context, req *bookingpb.CheckDatesRequest) (*bookingpb.CheckDatesResponse, error) {
	begin, err := time.Parse("2006-01-02", req.GetDateBegin())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Begining date error: %v", err))
	}

	end, err := time.Parse("2006-01-02", req.GetDateEnd())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Ending date error: %v", err))
	}

	roomId := req.GetRoomId()

	roomName := req.GetRoomName()

	hotelId := req.GetHotelId()

	bookingId := req.GetBookingId()

	diff := end.Sub(begin)

	counter := diff.Hours()

	ok := true
	
	for counter > 0 {
		ok = b.checkDate(begin, bookingId, roomId, roomName, hotelId)
		
		if !ok {
			break
		}
		
		begin = begin.Add(time.Hour * 24)
		counter -= 24
	}
	
	return &bookingpb.CheckDatesResponse{
		Status:               ok,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) CreateBookingInvoice(ctx context.Context, req *bookingpb.CreateBookingInvoiceRequest) (*bookingpb.CreateBookingInvoiceResponse, error) {
	bookings_id := req.BookingId
	date, err := time.Parse("2006-01-02", req.GetDate())

	if err != nil {
		date = time.Now()
	}

	year, _ := strconv.Atoi(date.Format("2006"))

	number := utils.GetNextNumberInvoice(0, int32(year), b.db)

	invoice := model.SaleInvoice{}

	invoice.Number = number
	invoice.Date = date

	bookings := []model.Booking{}

	if err := b.db.First(&bookings, "id IN (?)", bookings_id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	total := float32(0)

	for _, book := range bookings {
		total += book.Amount
	}

	invoice.Total = total

	client := model.Client{}

	if err := b.db.First(&client, bookings[0].ClientId).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	if client.Document == nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("No document in client: %v, %v %v", client.FirstName, client.LastName, *client.SecondLastName))
	}

	invoice.ClientId = client.Id
	invoice.Document = *client.Document
	invoice.FirstName = client.FirstName
	invoice.LastName = client.LastName
	invoice.SecondLastName = client.SecondLastName
	invoice.Email = client.Email
	invoice.Country = client.Country
	invoice.Region = client.Region
	invoice.City = client.City
	invoice.Address = client.Address
	invoice.PostalCode = client.PostalCode
	invoice.CellPhone = client.CellPhone
	invoice.HomePhone = client.HomePhone


	if err := b.db.Create(&invoice).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	//lines of the invoice

	return nil, nil
}

func toModel (book *bookingpb.Booking) model.Booking {
	return model.Booking{
		Id:       book.GetId(),
		Number:   book.GetNumber(),
		ClientId: book.GetClientId(),
		RoomId:   book.GetRoomId(),
		Amount:   book.GetAmount(),
	}
}

func toGrpc (book model.Booking) *bookingpb.BookingView {
	return &bookingpb.BookingView{
		Id:                   book.Id,
		Number:               book.Number,
		Client:               func () *bookingpb.ClientEntity {
			return &bookingpb.ClientEntity{
				Id:                   book.Client.Id,
				FirstName:            book.Client.FirstName,
				LastName:             func () string {
					if book.Client.LastName != nil {
						return *book.Client.LastName
					}

					return ""
				}(),
				SecondLastName:       func () string {
					if book.Client.SecondLastName != nil {
						return *book.Client.SecondLastName
					}

					return ""
				}(),
				BirthDate:            func () string {
					if book.Client.BirthDate != nil {
						return book.Client.BirthDate.Format("2006-01-02")
					}

					return ""
				}(),
				Document:             func () string {
					if book.Client.Document != nil {
						return *book.Client.Document
					}

					return ""
				}(),
				DocumentType:         func () string {
					if book.Client.DocumentType != nil {
						return *book.Client.DocumentType
					}

					return ""
				}(),
				Gender:               func () string {
					if book.Client.Gender != nil {
						return *book.Client.Gender
					}

					return ""
				}(),
				Address:              func () string {
					if book.Client.Address != nil {
						return *book.Client.Address
					}

					return ""
				}(),
				PostalCode:           func () string {
					if book.Client.PostalCode != nil {
						return *book.Client.PostalCode
					}

					return ""
				}(),
				Country:              func () string {
					if book.Client.Country != nil {
						return *book.Client.Country
					}

					return ""
				}(),
				Region:               func () string {
					if book.Client.Region != nil {
						return *book.Client.Region
					}

					return ""
				}(),
				City:                 func () string {
					if book.Client.City != nil {
						return *book.Client.City
					}

					return ""
				}(),
				HomePhone:            func () string {
					if book.Client.HomePhone != nil {
						return *book.Client.HomePhone
					}

					return ""
				}(),
				CellPhone:            func () string {
					if book.Client.CellPhone != nil {
						return *book.Client.CellPhone
					}

					return ""
				}(),
				Email:                func () string {
					if book.Client.Email != nil {
						return *book.Client.Email
					}

					return ""
				}(),
				CreditCardNumber:     func () string {
					if book.Client.CreditCardNumber != nil {
						return *book.Client.CreditCardNumber
					}

					return ""
				}(),
				CreditCardMonth:      func () string {
					if book.Client.CreditCardMonth != nil {
						return *book.Client.CreditCardMonth
					}

					return ""
				}(),
				CreditCardYear:       func () string {
					if book.Client.CreditCardYear != nil {
						return *book.Client.CreditCardYear
					}

					return ""
				}(),
				CreditCardSecurity:   func () string {
					if book.Client.CreditCardSecurity != nil {
						return *book.Client.CreditCardSecurity
					}

					return ""
				}(),
				Observations:         func () string {
					if book.Client.Observations != nil {
						return *book.Client.Observations
					}

					return ""
				}(),
				Provisional:          book.Client.Provisional,
				DocumentDate:         func () string {
					if book.Client.DocumentDate != nil {
						return book.Client.DocumentDate.Format("2006-01-02")
					}

					return ""
				}(),
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		Room:                 func () *bookingpb.RoomEntity {
			return &bookingpb.RoomEntity{
				Id:                   book.Room.Id,
				Name:                 book.Room.Name,
				Type:                 book.Room.Type.Name,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		Amount:               book.Amount,
		Dates:                func () []string {
			if len(book.Dates) == 0 {
				return []string{}
			}

			dtas := []string{}
			for _, date := range book.Dates {
				dtas = append(dtas, date.Date.Format("2006-01-02"))
			}

			return dtas
		}(),
		Checkin: 				func () string {
			if book.CheckIn != nil {
				return book.CheckIn.Format("2006-01-02 15:04:05")
			}

			return ""
		}(),
		DateBegin: 				book.DateBegin.Format("2006-01-02"),
		DateEnd: 				book.DateEnd.Format("2006-01-02"),
		InvoiceId:				func () int32 {
			if book.InvoiceId != nil {
				return *book.InvoiceId
			}

			return 0
		}(),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

func toGrpcList(book model.Booking) *bookingpb.BookingViewList {
	return &bookingpb.BookingViewList{
		Id:                   book.Id,
		Number:               book.Number,
		Client:               book.Client.FullName,
		Room:                 book.Room.Name,
		RoomType:             book.Room.Type.Name,
		DateBegin:            book.DateBegin.Format("2006-01-02"),
		DateEnd: 			  book.DateEnd.Format("2006-01-02"),
		CheckIn:              func () string {
			if book.CheckIn != nil {
				return book.CheckIn.Format("2006-01-02 15:04:05")
			}

			return ""
		}(),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

func (b *booking) checkDate (date time.Time, bookingId int32, roomId int32, roomName string, hotelId int32) bool {
	bookingDate := model.BookingDate{}

	if bookingId != 0 {
		if roomId != 0 {
			if err := b.db.Debug().Where("date = ? AND booking_id <> ? AND room_id = ?", date, bookingId, roomId).Find(&bookingDate).Error; err != nil {
				return true
			}
		}else {
			room := model.Room{}

			if err := b.db.Debug().First(&room, "name = ? AND hotel_id = ?", roomName, hotelId).Error; err != nil {
				return false
			}

			if err := b.db.Debug().Where("date = ? AND booking_id <> ? AND room_id = ?", date, bookingId, room.Id).Find(&bookingDate).Error; err != nil {
				return true
			}
		}
	}else {
		if roomId != 0 {
			if err := b.db.Debug().Where("date = ? AND room_id = ?", date, roomId).Find(&bookingDate).Error; err != nil {
				return true
			}
		}else {
			room := model.Room{}

			if err := b.db.Debug().First(&room, "name = ? AND hotel_id = ?", roomName, hotelId).Error; err != nil {
				return false
			}

			if err := b.db.Debug().Where("date = ? AND room_id = ?", date, room.Id).Find(&bookingDate).Error; err != nil {
				return true
			}
		}

	}

	return false
}

func (b *booking) CreateGuest(ctx context.Context, req *bookingpb.CreateGuestRequest) (*bookingpb.CreateGuestResponse, error) {
	item := req.GetGuest()
	
	guest := toModelGuest(item)
	
	guest.Created = time.Now()
	guest.Modified = time.Now()
	
	if err := b.db.Create(&guest).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &bookingpb.CreateGuestResponse{
		Guest:                toGrpcGuestView(guest),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) UpdateGuest(ctx context.Context, req *bookingpb.UpdateGuestRequest) (*bookingpb.UpdateGuestResponse, error) {
	item := req.GetGuest()

	guestDb := model.Guest{}

	if err := b.db.First(&guestDb, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	guest := toModelGuest(item)

	guest.Created = guestDb.Created
	guest.Modified = time.Now()

	if err := b.db.Save(&guest).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &bookingpb.UpdateGuestResponse{
		Guest:                toGrpcGuestView(guest),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) DeleteGuest(ctx  context.Context, req *bookingpb.DeleteGuestRequest) (*bookingpb.DeleteGuestResponse, error) {
	id := req.GetId()

	guestDb := model.Guest{}

	if err := b.db.First(&guestDb, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	
	if err := b.db.Unscoped().Delete(&guestDb).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &bookingpb.DeleteGuestResponse{
		Message:              "Operation ok!!!",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) GetGuest(ctx context.Context, req *bookingpb.GetGuestRequest) (*bookingpb.GetGuestResponse, error) {
	id := req.GetId()

	guestDb := model.Guest{}

	if err := b.db.First(&guestDb, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &bookingpb.GetGuestResponse{
		Guest:                toGrpcGuestView(guestDb),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *booking) FindGuests(ctx context.Context, req *bookingpb.FindGuestsRequest) (*bookingpb.FindGuestsResponse, error) {
	bookingId := req.GetBookingId()
	
	guests := []model.Guest{}
	
	if err := b.db.Order("id asc").Find(&guests, "booking_id = ?", bookingId).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	result := []*bookingpb.GuestView{}
	
	for _, guest := range guests {
		result = append(result, toGrpcGuestView(guest))
	}
	
	return &bookingpb.FindGuestsResponse{
		Guests:               result,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func toGrpcGuest (guest model.Guest) *bookingpb.Guest {
	return &bookingpb.Guest{
		Id:                   guest.Id,
		FirstName:            guest.FirstName,
		LastName:             guest.LastName,
		SecondLastName:       func () string {
			toReturn := ""
			if guest.SecondLastName != nil {
				toReturn = *guest.SecondLastName
			}

			return toReturn
		}(),
		BirthDate:            guest.BirthDate.Format("2006-01-02"),
		Document:             guest.Document,
		DocumentType:         guest.DocumentType,
		Gender:               guest.Gender,
		Country:              guest.Country,
		DocumentDate:         guest.DocumentType,
		BookingId: 			  guest.BookingId,
		CheckIn: 			  guest.CheckIn.Format("2006-01-02"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

func toGrpcGuestView (guest model.Guest) *bookingpb.GuestView {
	return &bookingpb.GuestView{
		Id:                   guest.Id,
		FirstName:            guest.FirstName,
		LastName:             guest.LastName,
		SecondLastName:       func () string {
			toReturn := ""
			if guest.SecondLastName != nil {
				toReturn = *guest.SecondLastName
			}

			return toReturn
		}(),
		BirthDate:            guest.BirthDate.Format("2006-01-02"),
		Document:             guest.Document,
		DocumentType:         guest.DocumentType,
		Gender:               guest.Gender,
		Country:              guest.Country,
		DocumentDate:         guest.DocumentDate.Format("2006-01-02"),
		Created: 			  guest.Created.Format("2006-01-02 "),
		Modified: 			  guest.Modified.Format("2006-01-02"),
		BookingId: 			  guest.BookingId,
		CheckIn: 			  guest.CheckIn.Format("2006-01-02"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

func toModelGuest (guest *bookingpb.Guest) model.Guest {
	return model.Guest{
		Id:             guest.GetId(),
		BookingId:      guest.GetBookingId(),
		FirstName:      guest.GetFirstName(),
		LastName:       guest.GetLastName(),
		SecondLastName: func () *string {
			if guest.GetSecondLastName() == "" {
				return nil
			}

			toReturn := guest.GetSecondLastName()

			return &toReturn
		}(),
		BirthDate:      func () time.Time {
			date, err := time.Parse("2006-01-02", guest.GetBirthDate())

			if err != nil {
				return time.Time{}
			}

			return date
		}(),
		Document:       guest.GetDocument(),
		DocumentType:   guest.GetDocumentType(),
		Gender:         guest.GetGender(),
		Country:        guest.GetCountry(),
		DocumentDate:   func () time.Time {
			date, err := time.Parse("2006-01-02", guest.GetDocumentDate())

			if err != nil {
				return time.Time{}
			}

			return date
		}(),
		CheckIn: 		func () time.Time {
			date, err := time.Parse("2006-01-02", guest.GetCheckIn())

			if err != nil {
				return time.Time{}
			}

			return date
		}(),
	}
}