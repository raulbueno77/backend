package product

import (
	model "ceginfor/cehotel-server/models"
	productpb "ceginfor/cehotel-server/proto/product"
	"context"
	"fmt"
	pagination2 "github.com/biezhi/gorm-paginator/pagination"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

type product struct {
	srv			*grpc.Server
	db			*gorm.DB
	paginator	int32
}

func NewProductService(srv *grpc.Server, db *gorm.DB, paginator int32) *product {
	return &product{
		srv:       srv,
		db:        db,
		paginator: paginator,
	}
}

func (p *product) RegisterService() {
	productpb.RegisterProductServicesServer(p.srv, p)
}

func (p *product) CreateProduct(ctx context.Context, req *productpb.CreateProductRequest) (*productpb.CreateProductResponse, error) {
	item := req.GetProduct()

	product := toModel(item)
	
	product.Created = time.Now()
	product.Modified = time.Now()
	
	if err := p.db.Create(&product).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &productpb.CreateProductResponse{
		Product:              toGrpc(product),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *product) UpdateProduct(ctx context.Context, req *productpb.UpdateProductRequest) (*productpb.UpdateProductResponse, error) {
	item := req.GetProduct()
	
	productDb := model.Product{}
	
	if err := p.db.First(&productDb, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	product := toModel(item)
	
	product.Created = productDb.Created
	product.Modified = time.Now()
	
	return &productpb.UpdateProductResponse{
		Product:              toGrpc(product),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *product) DeleteProduct(ctx context.Context, req *productpb.DeleteProductRequest) (*productpb.DeleteProductResponse, error) {
	id := req.GetId()

	product := model.Product{}

	if err := p.db.First(&product, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	if err := p.db.Unscoped().Delete(&product).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &productpb.DeleteProductResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	},nil
}

func (p *product) GetProduct(ctx context.Context, req *productpb.GetProductRequest) (*productpb.GetProductResponse, error) {
	id := req.GetId()

	product := model.Product{}

	if err := p.db.First(&product, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &productpb.GetProductResponse{
		Product:              toGrpc(product),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *product) FindProducts(ctx context.Context, req *productpb.FindProductsRequest) (*productpb.FindProductsResponse, error) {
	filter := req.GetFilter()
	pagination := req.GetPagination()

	sort := req.GetSort()

	if sort == "" {
		sort = "name"
	}

	direction := req.GetSortDirection()

	if direction == "" {
		direction = "ASC"
	}
	
	result := []*productpb.ProductView{}
	products := []model.Product{}

	bdd := p.db.Where("name like ? OR ref like ?", "%" + filter + "%", "%" + filter + "%")
	total_pages := int32(0)

	if pagination {
		page := req.GetPage()

		if page == 0 {
			page = 1
		}

		pag := pagination2.Paging(&pagination2.Param{
			DB:      bdd,
			Page:    int(page),
			Limit:   int(p.paginator),
			OrderBy: []string{sort + " " + direction},
			ShowSQL: true,
		}, &products)

		total_pages = int32(pag.TotalPage)
	}else {
		bdd.Find(&products).Order(sort + " " + direction)
	}

	for _, prod := range products {
		result = append(result, toGrpc(prod))
	}

	return &productpb.FindProductsResponse{
		Products:             result,
		TotalPages: 		  total_pages,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func toModel(prod *productpb.Product) model.Product {
	return model.Product{
		Id:       prod.GetId(),
		Ref:      prod.GetRef(),
		Name:     prod.GetName(),
		Price:    prod.GetPrice(),
	}
}

func toGrpc(prod model.Product) *productpb.ProductView {
	return &productpb.ProductView{
		Id:                   prod.Id,
		Ref:                  prod.Ref,
		Name:                 prod.Name,
		Price:                prod.Price,
		Created: 			  prod.Created.Format("2006-01-02 15:04:05"),
		Modified: 			  prod.Created.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}