package bank

import (
	model "ceginfor/cehotel-server/models"
	bankpb "ceginfor/cehotel-server/proto/bank"
	"context"
	"fmt"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

type bank struct {
	srv			*grpc.Server
	db			*gorm.DB
}

func NewBankService (srv *grpc.Server, db *gorm.DB) *bank {
	return &bank{
		srv: srv,
		db:  db,
	}
}

func (b *bank) RegisterServices () {
	bankpb.RegisterBankServicesServer(b.srv, b)
}

func (b *bank) CreateBank(ctx context.Context, req *bankpb.CreateBankRequest) (*bankpb.CreateBankResponse, error) {
	item := req.GetBank()
	
	bankDb := b.toModel(item)
	
	bankDb.Created = time.Now()
	bankDb.Modified = time.Now()
	
	if err := b.db.Create(&bankDb).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &bankpb.CreateBankResponse{
		Bank:                 b.toGrpc(bankDb),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *bank) UpdateBank(ctx context.Context, req *bankpb.UpdateBankRequest) (*bankpb.UpdateBankResponse, error) {
	item := req.GetBank()

	bankDbOld := model.Bank{}
	
	if err := b.db.First(&bankDbOld, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	bankDb := b.toModel(item)

	bankDb.Created = bankDbOld.Created
	bankDb.Modified = time.Now()

	if err := b.db.Save(&bankDb).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &bankpb.UpdateBankResponse{
		Bank:                 b.toGrpc(bankDb),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *bank) DeleteBank(ctx context.Context, req *bankpb.DeleteBankRequest) (*bankpb.DeleteBankResponse, error) {
	id := req.GetId()

	bankDb := model.Bank{}

	if err := b.db.First(&bankDb, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	if err := b.db.Unscoped().Delete(&bankDb).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &bankpb.DeleteBankResponse{
		Message:              "Operation ok!!",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *bank) GetBank(ctx context.Context, req *bankpb.GetBankRequest) (*bankpb.GetBankResponse, error) {
	id := req.GetId()

	bankDb := model.Bank{}

	if err := b.db.First(&bankDb, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &bankpb.GetBankResponse{
		Bank:                 b.toGrpc(bankDb),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *bank) FindBanks(ctx context.Context, req *bankpb.FindBanksRequest) (*bankpb.FindBanksResponse, error) {
	banks := []model.Bank{}
	
	if err := b.db.Order("id desc").Find(&banks).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	banksReturn := []*bankpb.BankView{}
	
	for _, bnk := range banks {
		banksReturn = append(banksReturn, b.toGrpc(bnk))
	}
	
	return &bankpb.FindBanksResponse{
		Bank:                 banksReturn,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (b *bank) toModel (bnk *bankpb.Bank) model.Bank {
	return model.Bank{
		Id:       bnk.GetId(),
		Name:     bnk.GetName(),
		Cash:     bnk.GetCash(),
		Number:   func () *string {
			if bnk.GetNumber() == "" {
				return nil
			}
			toReturn := bnk.GetNumber()
			
			return &toReturn
		}(),
	}
}

func (b *bank) toGrpc (bnk model.Bank) *bankpb.BankView {
	return &bankpb.BankView{
		Id:                   bnk.Id,
		Name:                 bnk.Name,
		Cash:                 bnk.Cash,
		Number:               func () string {
			if bnk.Number == nil {
				return ""
			}
			
			return *bnk.Number
		}(),
		Created:              bnk.Created.Format("2006-01-02 15:04:05"),
		Modified:             bnk.Modified.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}