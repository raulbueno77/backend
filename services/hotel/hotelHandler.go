package hotel

import (
	"bufio"
	"bytes"
	model "ceginfor/cehotel-server/models"
	hotelpb "ceginfor/cehotel-server/proto/hotel"
	"ceginfor/cehotel-server/utils"
	"context"
	"fmt"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"os"
	"strings"
	"time"
)

type hotel struct {
	srv		*grpc.Server
	db		*gorm.DB
	maxUploadSize	float32
	resolutions		map[string]int
}

func NewHotelService(srv *grpc.Server, db *gorm.DB, maxUploadSize float32, resolutions map[string]int) *hotel {
	return &hotel{
		srv: srv,
		db:  db,
		maxUploadSize: maxUploadSize,
		resolutions: resolutions,
	}
}

func (h *hotel) RegisterService() {
	hotelpb.RegisterHotelServicesServer(h.srv, h)
}

func (h *hotel) CreateHotel(ctx context.Context, req *hotelpb.CreateHotelRequest) (*hotelpb.CreateHotelResponse, error) {
	item := req.GetHotel()

	hotel := toModel(item)
	
	hotel.Created = time.Now()
	hotel.Modified = time.Now()
	
	if err := h.db.Create(&hotel).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &hotelpb.CreateHotelResponse{
		Hotel:                toView(hotel),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (h *hotel) UpdateHotel(ctx context.Context, req *hotelpb.UpdateHotelRequest) (*hotelpb.UpdateHotelResponse, error) {
	item := req.GetHotel()

	hotel := toModel(item)

	hot := model.Hotel{}
	
	if err := h.db.First(&hot, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	hotel.Created = hot.Created
	hotel.Modified = time.Now()

	if err := h.db.Save(&hotel).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &hotelpb.UpdateHotelResponse{
		Hotel:                toView(hotel),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (h *hotel) DeleteHotel(ctx context.Context, req *hotelpb.DeleteHotelRequest) (*hotelpb.DeleteHotelResponse, error) {
	id := req.GetId()
	
	hotel := model.Hotel{}

	if err := h.db.First(&hotel, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	if err := h.db.Unscoped().Delete(&hotel).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &hotelpb.DeleteHotelResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (h *hotel) GetHotel(ctx context.Context, req *hotelpb.GetHotelRequest) (*hotelpb.GetHotelResponse, error) {
	id := req.GetId()

	hotel := model.Hotel{}

	if err := h.db.First(&hotel, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &hotelpb.GetHotelResponse{
		Hotel:                toView(hotel),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (h *hotel) FindHotel(ctx context.Context, req *hotelpb.FindHotelRequest) (*hotelpb.FindHotelResponse, error) {
	ftr := req.GetFilter()
	idUser := req.GetUserId()

	hotels := []*model.Hotel{}

	if idUser != 0 {
		user := model.Users{}
		if err := h.db.Debug().Preload("Hotels").Find(&user, idUser).Error; err != nil {

			return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
		}else {
			log.Println("hola")
			if user.Hotels != nil {
				hotels = user.Hotels
			}
		}
	}else {
		if err := h.db.Where("name like ?", "%" + ftr + "%").Order("id ASC").Find(&hotels).Error; err != nil {
			return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
		}
	}

	result := []*hotelpb.HotelView{}
	
	for _, ht := range hotels {
		result = append(result, toView(*ht))
	}
	
	return &hotelpb.FindHotelResponse{
		Hotels:               result,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (h *hotel) UploadLogo(stream hotelpb.HotelServices_UploadLogoServer) error {
	req, err := stream.Recv()

	os.Mkdir("media", os.ModePerm)

	if err != nil {
		return status.Error(codes.Internal, fmt.Sprintf("No info reqtrieved %v", err))
	}

	filesize := 0
	filename := req.GetPictureData().GetFilename()
	hotelId := req.GetPictureData().GetHotelId()
	fileData := bytes.Buffer{}

	splitedFilename := strings.Split(filename, ".")

	extension := strings.ToLower(splitedFilename[len(splitedFilename) - 1])

	if extension != "jpg" && extension != "jpeg" && extension != "png" {
		return status.Error(codes.InvalidArgument, fmt.Sprintf("Not valid format"))
	}

	newFilename := utils.RandomLowerString(12) + "." + extension

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			return status.Error(codes.Internal, fmt.Sprintf("Error uploading the file %v", err))
		}

		chunk := req.GetContent()
		size := len(chunk)

		filesize += size

		if float32(filesize) / (1024 * 1024) > h.maxUploadSize {
			log.Printf("Max upload size exceed %v", float32(filesize) / (1024 * 1024))
			return status.Error(codes.InvalidArgument, fmt.Sprintf("it has exceed the upload limit size of %vM!!!", h.maxUploadSize))
		}

		_, err2 := fileData.Write(chunk)

		if err2 != nil {
			log.Println(err2)
			return status.Error(codes.Internal, fmt.Sprintf("Error uploading the file %v", err))
		}
	}

	log.Println(filesize)

	if filesize > 0 {
		fileToSave, err := os.Create("media/" + newFilename)

		if err != nil {
			return status.Error(codes.Internal, fmt.Sprintf("Error creating file %v", err))
		}
		defer fileToSave.Close()

		fileData.WriteTo(fileToSave)

		hotel := model.Hotel{}

		if err := h.db.First(&hotel, hotelId).Error; err != nil {
			return status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
		}

		hotel.Logo = &newFilename
		hotel.Modified = time.Now()

		h.db.Save(&hotel)

		//generating thumbnails for the picture
		go func() {
			for index, value := range h.resolutions {
				os.Mkdir("./media/" + index, os.ModePerm)
				utils.GenerateThumbnail(newFilename, value, index)
			}
		}()

		stream.SendAndClose(&hotelpb.UploadLogoHotelResponse{
			Filename:             newFilename,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})
	}else {
		stream.SendAndClose(&hotelpb.UploadLogoHotelResponse{
			Filename:             "",
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		})
	}

	return nil
}

func (h *hotel) DeleteLogo(ctx context.Context, req *hotelpb.DeleteLogoRequest) (*hotelpb.DeleteLogoResponse, error) {
	hotelId := req.GetHotelId()

	hotel := model.Hotel{}

	if err := h.db.First(&hotel, hotelId).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	if hotel.Logo != nil {
		os.Remove("media/" + *hotel.Logo)

		for index, _ := range h.resolutions {
			os.Remove("media/" + index + "/" + *hotel.Logo)
		}
	}

	hotel.Logo = nil
	hotel.Modified = time.Now()

	h.db.Save(&hotel)

	return &hotelpb.DeleteLogoResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (h *hotel) GetLogo(req *hotelpb.GetLogoRequest, stream hotelpb.HotelServices_GetLogoServer) error {
	hotelId := req.GetHotelId()

	hotel := model.Hotel{}

	if err := h.db.First(&hotel, hotelId).Error; err != nil {
		return status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	if hotel.Logo == nil {
		return status.Error(codes.NotFound, fmt.Sprintf("No Logo"))
	}

	file, err := os.Open("media/" + *hotel.Logo)

	if err != nil {
		return status.Error(codes.NotFound, fmt.Sprintf("Error reading file: %v", err))
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	buffer := make([]byte, 1024)

	for {
		n, err := reader.Read(buffer)
		log.Println(n)
		if err == io.EOF {
			break
		}
		if err != nil {
			return status.Error(codes.NotFound, fmt.Sprintf("Cannot read chunk to buffer: %v", err))
		}

		res := &hotelpb.GetLogoResponse{
			Data:                 &hotelpb.GetLogoResponse_Content{Content: buffer[:n]},
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		}

		err = stream.Send(res)
		if err != nil {
			return status.Error(codes.NotFound, fmt.Sprintf("Error sending content file: %v", err))
		}
	}

	res := &hotelpb.GetLogoResponse{
		Data:                 &hotelpb.GetLogoResponse_Filename{Filename: *hotel.Logo},
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	err = stream.Send(res)

	if err != nil {
		return err
	}

	return nil

	return nil
}

func toModel(hotel *hotelpb.Hotel) model.Hotel {
	return model.Hotel{
		Id:         hotel.GetId(),
		Name:      hotel.GetName(),
		Document:   hotel.GetDocument(),
		Reference:  hotel.GetReference(),
		Phone:      hotel.GetPhone(),
		Email:      hotel.GetEmail(),
		Address:    hotel.GetAddress(),
		PostalCode: hotel.GetPostalCode(),
		Country:    hotel.GetCountry(),
		Region:     hotel.GetRegion(),
		City:       hotel.GetCity(),
		Logo: 		func () *string {
			if hotel.GetLogo() != "" {
				toReturn := hotel.GetLogo()

				return &toReturn
			}

			return nil
		}(),
		Created:    time.Time{},
		Modified:   time.Time{},
	}
}

func toGrpc(hotel model.Hotel) *hotelpb.Hotel {
	return &hotelpb.Hotel{
		Id:                   hotel.Id,
		Name:                 hotel.Name,
		Document:             hotel.Document,
		Reference:            hotel.Reference,
		Phone:                hotel.Phone,
		Email:                hotel.Email,
		Address:              hotel.Address,
		PostalCode:           hotel.PostalCode,
		Country:              hotel.Country,
		Region:               hotel.Region,
		City:                 hotel.City,
		Logo:				  func () string {
			if hotel.Logo != nil {
				return *hotel.Logo
			}

			return ""
		}(),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

func toView(hotel model.Hotel) *hotelpb.HotelView {
	return &hotelpb.HotelView{
		Id:                   hotel.Id,
		Name:                 hotel.Name,
		Document:             hotel.Document,
		Reference:            hotel.Reference,
		Phone:                hotel.Phone,
		Email:                hotel.Email,
		Address:              hotel.Address,
		PostalCode:           hotel.PostalCode,
		Country:              hotel.Country,
		Region:               hotel.Region,
		City:                 hotel.City,
		Logo:				  func () string {
			if hotel.Logo != nil {
				return *hotel.Logo
			}

			return ""
		}(),
		Created:              hotel.Created.Format("2006-01-02 15:04:05"),
		Modifed:              hotel.Modified.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}