package expense

import (
	model "ceginfor/cehotel-server/models"
	expenseincomepb "ceginfor/cehotel-server/proto/expense_income"
	"context"
	"fmt"
	"github.com/biezhi/gorm-paginator/pagination"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"strconv"
	"time"
)

type expense struct {
	srv			*grpc.Server
	db 			*gorm.DB
	paginator 	int32
}

func NewExpenseServices(srv *grpc.Server, db *gorm.DB, paginator int32) *expense {
	return &expense{
		srv:       srv,
		db:        db,
		paginator: paginator,
	}
}

func (e *expense) RegisterServices() {
	expenseincomepb.RegisterExpenseServicesServer(e.srv, e)
}

func (e *expense) CreateExpense(ctx context.Context, req *expenseincomepb.CreateExpenseRequest) (*expenseincomepb.CreateExpenseResponse, error) {
	item := req.GetExpense()

	expense := ExpenseToModel(item)

	expense.Created = time.Now()
	expense.Modified = time.Now()

	if err := e.db.Create(&expense).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	if err := e.db.Preload("PaymentMethod").Preload("ExpenseType").First(&expense, expense.Id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	return &expenseincomepb.CreateExpenseResponse{
		Expense:              ExpenseToGrpc(expense),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (e *expense) UpdateExpense(ctx context.Context, req *expenseincomepb.UpdateExpenseRequest) (*expenseincomepb.UpdateExpenseResponse, error) {
	item := req.GetExpense()

	expenseDb := model.Expense{}

	if err := e.db.First(&expenseDb, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	expense := ExpenseToModel(item)

	expense.Created = expenseDb.Created
	expense.Modified = time.Now()

	if err := e.db.Save(&expense).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	if err := e.db.Preload("PaymentMethod").Preload("ExpenseType").First(&expense, expense.Id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	return &expenseincomepb.UpdateExpenseResponse{
		Expense:              ExpenseToGrpc(expense),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (e *expense) DeleteExpense(ctx context.Context, req *expenseincomepb.DeleteExpenseRequest) (*expenseincomepb.DeleteExpenseResponse, error) {
	id := req.GetId()

	expense := model.Expense{}

	if err := e.db.First(&expense, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	if err := e.db.Unscoped().Delete(&expense).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &expenseincomepb.DeleteExpenseResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (e *expense) GetExpense(ctx context.Context, req *expenseincomepb.GetExpenseRequest) (*expenseincomepb.GetExpenseResponse, error) {
	id := req.GetId()

	expense := model.Expense{}

	if err := e.db.Preload("PaymentMethod").Preload("ExpenseType").First(&expense, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	return &expenseincomepb.GetExpenseResponse{
		Expense:              ExpenseToGrpc(expense),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (e *expense) FindExpenses(ctx context.Context, req *expenseincomepb.FindExpensesRequest) (*expenseincomepb.FindExpensesResponse, error) {
	begin, err := time.Parse("2006-01-02 15:04:05", req.GetDateBegin())
	if err != nil {
		begin, err = time.Parse("2006-01-02 15:04:05", req.GetDateBegin() + " 00:00:00")
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Begining date error: %v", err))
		}
	}

	end, err := time.Parse("2006-01-02 15:04:05", req.GetDateEnd())
	if err != nil {
		end, err = time.Parse("2006-01-02 15:04:05", req.GetDateEnd() + " 23:59:59")
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Ending date error: %v", err))
		}
	}

	filter := req.GetFilter()

	typeId := req.GetExpenseTypeId()

	methodId := req.GetPaymentMethodId()
	
	page := req.GetPage()
	
	if page == 0 {
		page = 1
	}
	
	sort := req.GetSort()
	
	if sort == "" {
		sort = "expenses.date"
	}
	
	direction := req.GetSortDirection()
	
	if direction == "" {
		direction = "ASC"
	}
	
	where := "1 = 1"
	
	if typeId != 0  {
		where += " AND expenses.expense_type_id = " + strconv.Itoa(int(typeId))
	}
	
	if methodId != 0 {
		where += " AND expenses.payment_method_id = " + strconv.Itoa(int(methodId))
	}
 
	where += " AND expenses.description like ? AND expenses.date BETWEEN ? AND ?"
	
	db := e.db.Table("expenses").Select("expenses.*").Joins("join expense_types on expenses.expense_type_id = expense_types.id").Joins("join payment_methods on expenses.payment_method_id = payment_methods.id").Preload("PaymentMethod").Preload("ExpenseType").Where(where, "%" + filter + "%", begin, end)

	expenses := []model.Expense{}
	
	pag := pagination.Paging(&pagination.Param{
		DB:      db,
		Page:    int(page),
		Limit:   int(e.paginator),
		OrderBy: []string{sort + " " + direction},
		ShowSQL: true,
	}, &expenses)
	
	result := []*expenseincomepb.ExpenseView{}
	totalAmount := float32(0.0)

	for _, exp := range expenses {
		totalAmount += exp.Amount
		result = append(result, ExpenseToGrpc(exp))
	}
	
	return &expenseincomepb.FindExpensesResponse{
		Expenses:             result,
		TotalPages:           int32(pag.TotalPage),
		TotalAmount:          totalAmount,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (e *expense) CreateExpenseType(ctx context.Context, req *expenseincomepb.CreateExpenseTypeRequest) (*expenseincomepb.CreateExpenseTypeResponse, error) {
	item := req.GetType()

	etype := ExpenseTypeToModel(item)

	etype.Created = time.Now()
	etype.Modified = time.Now()

	if err := e.db.Create(&etype).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &expenseincomepb.CreateExpenseTypeResponse{
		Type:                 ExpenseTypeToGrpc(etype),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (e *expense) UpdateExpenseType(ctx context.Context, req *expenseincomepb.UpdateExpenseTypeRequest) (*expenseincomepb.UpdateExpenseTypeResponse, error) {
	item := req.GetType()

	etypeDb := model.ExpenseType{}

	if err := e.db.First(&etypeDb, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	etype := ExpenseTypeToModel(item)

	etype.Created = etypeDb.Created
	etype.Modified = time.Now()

	if err := e.db.Save(&etype).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &expenseincomepb.UpdateExpenseTypeResponse{
		Type:                 ExpenseTypeToGrpc(etype),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (e *expense) DeleteExpenseType(ctx context.Context, req *expenseincomepb.DeleteExpenseTypeRequest) (*expenseincomepb.DeleteExpenseTypeResponse, error) {
	id := req.GetId()

	etype := model.ExpenseType{}

	if err := e.db.First(&etype, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	if err := e.db.Unscoped().Delete(&etype).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &expenseincomepb.DeleteExpenseTypeResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (e *expense) GetExpenseType(ctx context.Context, req *expenseincomepb.GetExpenseTypeRequest) (*expenseincomepb.GetExpenseTypeResponse, error) {
	id := req.GetId()

	etype := model.ExpenseType{}

	if err := e.db.First(&etype, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	return &expenseincomepb.GetExpenseTypeResponse{
		Type:                 ExpenseTypeToGrpc(etype),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (e *expense) FindExpenseTypes(ctx context.Context, req *expenseincomepb.FindExpenseTypesRequest) (*expenseincomepb.FindExpenseTypesResponse, error) {
	filter := req.GetFilter()

	etypes := []model.ExpenseType{}

	if err := e.db.Where("name like ?", "%" + filter + "%").Find(&etypes).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	result := []*expenseincomepb.ExpenseTypeView{}

	for _, et := range etypes {
		result = append(result, ExpenseTypeToGrpc(et))
	}

	return &expenseincomepb.FindExpenseTypesResponse{
		Types:                result,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func ExpenseTypeToModel(etype *expenseincomepb.ExpenseType) model.ExpenseType {
	return model.ExpenseType{
		Id:       etype.GetId(),
		Name:     etype.GetName(),
	}
}

func ExpenseTypeToGrpc(etype model.ExpenseType) *expenseincomepb.ExpenseTypeView {
	return &expenseincomepb.ExpenseTypeView{
		Id:                   etype.Id,
		Name:                 etype.Name,
		Created:              etype.Created.Format("2006-01-02 15:04:05"),
		Modified:             etype.Created.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

func ExpenseToModel(exp *expenseincomepb.Expense) model.Expense {
	return model.Expense{
		Id:              exp.GetId(),
		Date:            func () time.Time {
			date, err := time.Parse("2006-01-02 15:04:05", exp.GetDate())

			if err != nil {
				return time.Time{}
			}

			return date
		}(),
		PaymentMethodId: exp.GetPaymentMethodId(),
		ExpenseTypeId:   exp.GetExpenseTypeId(),
		Description:     exp.GetDescription(),
		Amount:          exp.GetAmount(),
	}
}

func ExpenseToGrpc(exp model.Expense) *expenseincomepb.ExpenseView {
	return &expenseincomepb.ExpenseView{
		Id:                   exp.Id,
		Date:                 exp.Date.Format("2006-01-02 15:04:05"),
		PaymentMethod:        func () *expenseincomepb.PaymentMethodView {
			return &expenseincomepb.PaymentMethodView{
				Id:                   exp.PaymentMethod.Id,
				Name:                 exp.PaymentMethod.Name,
				Created:              exp.PaymentMethod.Created.Format("2006-01-02 15:04:05"),
				Modified:             exp.PaymentMethod.Modified.Format("2006-01-02 15:04:05"),
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		ExpenseType:          func () *expenseincomepb.ExpenseTypeView {
			return &expenseincomepb.ExpenseTypeView{
				Id:                   exp.ExpenseType.Id,
				Name:                 exp.ExpenseType.Name,
				Created:              exp.Created.Format("2006-01-02 15:04:05"),
				Modified:             exp.Modified.Format("2006-01-02 15:04:05"),
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			}
		}(),
		Description:          exp.Description,
		Amount:               exp.Amount,
		Created:              exp.Created.Format("2006-01-02 15:04:05"),
		Modified:             exp.Modified.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

