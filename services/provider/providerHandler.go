package provider

import (
	model "ceginfor/cehotel-server/models"
	providerpb "ceginfor/cehotel-server/proto/provider"
	"context"
	"fmt"
	"github.com/biezhi/gorm-paginator/pagination"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

type provider struct {
	srv				*grpc.Server
	db				*gorm.DB
	paginator		int32
}

func NewProviderService (srv *grpc.Server, db *gorm.DB, paginator int32) *provider {
	return &provider{
		srv:       srv,
		db:        db,
		paginator: paginator,
	}
}

func (p *provider) RegisterServices () {
	providerpb.RegisterProviderServicesServer(p.srv, p)
}

func (p *provider) CreateProvider(ctx context.Context, req *providerpb.CreateProviderRequest) (*providerpb.CreateProviderResponse, error) {
	item := req.GetProvider()

	provider := p.toModel(item)

	provider.Created = time.Now()
	provider.Modified = time.Now()

	if err := p.db.Create(&provider).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &providerpb.CreateProviderResponse{
		Provider:             p.toView(provider),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *provider) UpdateProvider(ctx context.Context, req *providerpb.UpdateProviderRequest) (*providerpb.UpdateProviderResponse, error) {
	item := req.GetProvider()

	provider := p.toModel(item)

	providerDb := model.Provider{}

	if err := p.db.First(&providerDb, item.Id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	provider.Created = providerDb.Created
	provider.Modified = time.Now()

	if err := p.db.Save(&provider).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &providerpb.UpdateProviderResponse{
		Provider:             p.toView(provider),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *provider) DeleteProvider(ctx context.Context, req *providerpb.DeleteProviderRequest) (*providerpb.DeleteProviderResponse, error) {
	id := req.GetId()

	providerDb := model.Provider{}

	if err := p.db.First(&providerDb, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	if err := p.db.Unscoped().Delete(&providerDb).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}

	return &providerpb.DeleteProviderResponse{
		Message:              "Operation ok!!!",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *provider) GetProvider(ctx context.Context, req *providerpb.GetProviderRequest) (*providerpb.GetProviderResponse, error) {
	id := req.GetId()

	providerDb := model.Provider{}

	if err := p.db.First(&providerDb, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}

	return &providerpb.GetProviderResponse{
		Provider:             p.toView(providerDb),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *provider) FindProviders(ctx context.Context, req *providerpb.FindProvidersRequest) (*providerpb.FindProvidersResponse, error) {
	filter := "%" + req.GetFilter() + "%"
	page := req.GetPage()

	if page == 0 {
		page = 1
	}

	sort := req.GetSort()

	if sort == "" {
		sort = "id"
	}

	direction := req.GetSortDirection()

	if direction == "" {
		direction = "asc"
	}

	providers := []model.Provider{}

	pag := pagination.Paging(&pagination.Param{
		DB:      p.db.Where("document like ? OR name like ? OR country like ? OR region like ? OR city like ? or job_phone like ? or cell_phone like ? or email like ?", filter, filter, filter, filter, filter, filter, filter, filter),
		Page:    int(page),
		Limit:   int(p.paginator),
		OrderBy: []string{sort + " " + direction},
		ShowSQL: true,
	}, &providers)

	result := []*providerpb.ProviderView{}

	for _, prov := range providers {
		result = append(result, p.toView(prov))
	}

	return &providerpb.FindProvidersResponse{
		Providers:            result,
		TotalPages:           int32(pag.TotalPage),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *provider) toView(prov model.Provider) *providerpb.ProviderView {
	return &providerpb.ProviderView{
		Id:                   prov.Id,
		Name:                 prov.Name,
		Document:             func () string {
			if prov.Document != nil {
				return *prov.Document
			}

			return ""
		}(),
		Address:              func () string {
			if prov.Address != nil {
				return *prov.Address
			}

			return ""
		}(),
		PostalCode:           func () string {
			if prov.PostalCode != nil {
				return *prov.PostalCode
			}

			return ""
		}(),
		Country:              func () string {
			if prov.Country != nil {
				return *prov.Country
			}

			return ""
		}(),
		Region:               func () string {
			if prov.Region != nil {
				return *prov.Region
			}

			return ""
		}(),
		City:                 func () string {
			if prov.City != nil {
				return *prov.City
			}

			return ""
		}(),
		JobPhone:             func () string {
			if prov.JobPhone != nil {
				return *prov.JobPhone
			}

			return ""
		}(),
		CellPhone:            func () string {
			if prov.CellPhone != nil {
				return *prov.CellPhone
			}

			return ""
		}(),
		Email:                func () string {
			if prov.Email != nil {
				return *prov.Email
			}

			return ""
		}(),
		Observations:        func () string {
			if prov.Observations != nil {
				return *prov.Observations
			}

			return ""
		}(),
		Created:              prov.Created.Format("2006-01-02 15:04:05"),
		Modified:             prov.Modified.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}

func (p *provider) toModel(prov *providerpb.Provider) model.Provider  {
	return model.Provider{
		Id:           prov.GetId(),
		Name:         prov.GetName(),
		Document:     func () *string {
			if prov.GetDocument() != "" {
				value := prov.GetDocument()
				return &value
			}

			return nil
		}(),
		Address:      func () *string {
			if prov.GetAddress() != "" {
				value := prov.GetAddress()
				return &value
			}

			return nil
		}(),
		PostalCode:   func () *string {
			if prov.GetPostalCode() != "" {
				value := prov.GetPostalCode()
				return &value
			}

			return nil
		}(),
		Country:      func () *string {
			if prov.GetCountry() != "" {
				value := prov.GetCountry()
				return &value
			}

			return nil
		}(),
		Region:       func () *string {
			if prov.GetRegion() != "" {
				value := prov.GetRegion()
				return &value
			}

			return nil
		}(),
		City:         func () *string {
			if prov.GetCity() != "" {
				value := prov.GetCity()
				return &value
			}

			return nil
		}(),
		JobPhone:     func () *string {
			if prov.GetJobPhone() != "" {
				value := prov.GetJobPhone()
				return &value
			}

			return nil
		}(),
		CellPhone:    func () *string {
			if prov.GetCellPhone() != "" {
				value := prov.GetCellPhone()
				return &value
			}

			return nil
		}(),
		Email:        func () *string {
			if prov.GetEmail() != "" {
				value := prov.GetEmail()
				return &value
			}

			return nil
		}(),
		Observations: func () *string {
			if prov.GetObservations() != "" {
				value := prov.GetObservations()
				return &value
			}

			return nil
		}(),
	}
}