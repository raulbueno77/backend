package paymentMethod

import (
	model "ceginfor/cehotel-server/models"
	expenseincomepb "ceginfor/cehotel-server/proto/expense_income"
	"context"
	"fmt"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

type paymentMethod struct {
	srv		*grpc.Server
	db		*gorm.DB
}

func NewPaymentMethodService(srv *grpc.Server, db *gorm.DB) *paymentMethod {
	return &paymentMethod{
		srv: srv,
		db:  db,
	}
}

func (p *paymentMethod) RegisterServices() {
	expenseincomepb.RegisterPaymentMethodServicesServer(p.srv, p)
}

func (p *paymentMethod) CreatePaymentMethod(ctx context.Context, req *expenseincomepb.CreatePaymentMethodRequest) (*expenseincomepb.CreatePaymentMethodResponse, error) {
	item := req.GetPaymentMethod()
	
	payment := toModel(item)
	
	payment.Created = time.Now()
	payment.Modified = time.Now()
	
	if err := p.db.Create(&payment).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &expenseincomepb.CreatePaymentMethodResponse{
		PaymentMethod:        toGrpc(payment),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *paymentMethod) UpdatePaymentMethod(ctx context.Context, req *expenseincomepb.UpdatePaymentMethodRequest) (*expenseincomepb.UpdatePaymentMethodResponse, error) {
	item := req.GetPaymentMethod()
	
	payment := toModel(item)
	
	paymentDb := model.PaymentMethod{}
	
	if err := p.db.First(&paymentDb, item.GetId()).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	payment.Created = paymentDb.Created
	payment.Modified = time.Now()

	if err := p.db.Save(&payment).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}


	return &expenseincomepb.UpdatePaymentMethodResponse{
		PaymentMethod:        toGrpc(payment),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *paymentMethod) DeletePaymentMethod(ctx context.Context, req *expenseincomepb.DeletePaymentMethodRequest) (*expenseincomepb.DeletePaymentMethodResponse, error) {
	id := req.GetId()

	payment := model.PaymentMethod{}

	if err := p.db.First(&payment, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	if err := p.db.Unscoped().Delete(&payment).Error; err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &expenseincomepb.DeletePaymentMethodResponse{
		Status:               true,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *paymentMethod) GetPaymentMethod(ctx context.Context, req *expenseincomepb.GetPaymentMethodRequest) (*expenseincomepb.GetPaymentMethodResponse, error) {
	id := req.GetId()

	payment := model.PaymentMethod{}

	if err := p.db.First(&payment, id).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	return &expenseincomepb.GetPaymentMethodResponse{
		PaymentMethod:        toGrpc(payment),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func (p *paymentMethod) FindPaymentMethods(ctx context.Context, req *expenseincomepb.FindPaymentMethodsRequest) (*expenseincomepb.FindPaymentMethodsResponse, error) {
	filter := req.GetFilter()
	
	payments := []model.PaymentMethod{}
	
	if err := p.db.Where("name like ?", "%" + filter + "%").Find(&payments).Error; err != nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("Database Error: %v", err))
	}
	
	result := []*expenseincomepb.PaymentMethodView{}
	
	for _, pay := range payments {
		result = append(result, toGrpc(pay))
	}
	
	return &expenseincomepb.FindPaymentMethodsResponse{
		PaymentMethods:       result,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}

func toModel(pay *expenseincomepb.PaymentMethod) model.PaymentMethod {
	return model.PaymentMethod{
		Id:       pay.GetId(),
		Name:     pay.GetName(),
	}
}

func toGrpc(pay model.PaymentMethod) *expenseincomepb.PaymentMethodView {
	return &expenseincomepb.PaymentMethodView{
		Id:                   pay.Id,
		Name:                 pay.Name,
		Created:              pay.Created.Format("2006-01-02 15:04:05"),
		Modified:             pay.Modified.Format("2006-01-02 15:04:05"),
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
}