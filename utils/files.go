package utils

import (
	"fmt"
	"image/gif"
	"image/jpeg"
	"image/png"
	"log"
	"os"
	"strings"
	"github.com/nfnt/resize"
)

func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func GenerateThumbnail(picture string, resolution int, folder string) (bool, string) {
	log.Printf("Generatin thumbnails for: %v", picture)
	file, err := os.Open("./media/" + picture)
	defer file.Close()

	if err != nil {
		return false, fmt.Sprintf("%v", err)
	}

	aux := strings.Split(picture, ".")

	ext := aux[len(aux) - 1]

	new_file := aux[0] + "." + aux[len(aux) - 1]

	switch ext {
	case "jpg", "jpeg":
		img, err := jpeg.Decode(file)

		if err != nil {
			return false, fmt.Sprintf("%v", err)
		}

		m := resize.Resize(0, uint(resolution), img, resize.Lanczos3)

		out, err := os.Create("./media/" + folder + "/" + new_file)
		if err != nil {
			return false, fmt.Sprintf("%v", err)
		}
		defer out.Close()

		// write new image to file
		jpeg.Encode(out, m, nil)

		break
	case "png":
		img, err := png.Decode(file)

		if err != nil {
			return false, fmt.Sprintf("%v", err)
		}

		m := resize.Resize(0, uint(resolution), img, resize.Lanczos3)

		out, err := os.Create("./media/" + folder + "/" + new_file)
		if err != nil {
			return false, fmt.Sprintf("%v", err)
		}
		defer out.Close()

		// write new image to file
		png.Encode(out, m)
		break
	case "gif":
		img, err := gif.Decode(file)

		if err != nil {
			return false, fmt.Sprintf("%v", err)
		}

		m := resize.Resize(0, uint(resolution), img, resize.Lanczos3)

		out, err := os.Create("./media/" + folder + "/" + new_file)
		if err != nil {
			return false, fmt.Sprintf("%v", err)
		}
		defer out.Close()

		gif.Encode(out, m, nil)
		break

	}

	return true, new_file
}


