package model

import "time"

type Task struct {
	Id			int32		`gorm:"primary_key" json:"id"`
	Date		time.Time	`gorm:"not null" json:"date"`
	RoomId		*int32		`json:"room_id"`
	Room		*Room		`json:"room"`
	UserId		int32		`gorm:"primary_key" json:"user_id"`
	User		Users		`json:"user"`
	TaskTypeId	int32		`gorm:"not null" json:"task_type_id"`
	TaskType	TaskType	`json:"task_type"`
	Description	*string		`gorm:"size=999999" json:"description"`
	Created		time.Time 	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
}

type TaskType struct {
	Id			int32		`gorm:"primary_key" json:"id"`
	Name		string		`gorm:"not null" json:"name"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
}
