package model

import "time"

type Attribute struct {
	Id			int32	`gorm:"primary_key" json:"id"`
	Name		string	`gorm:"size:50;not null" json:"name"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
}
