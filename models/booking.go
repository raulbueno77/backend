package model

import "time"

type Booking struct {
	Id			int32			`gorm:"primary_key" json:"id"`
	Number		int32			`gorm:"not null" json:"number"`
	Dates		[]BookingDate	`json:"dates"`
	ClientId	int32			`gorm:"not null"`
	Client		Client			`json:"client"`
	RoomId		int32			`gorm:"not null"`
	Room		Room			`json:"room"`
	Amount		float32			`gorm:"not null" json:"amount"`
	CheckIn		*time.Time		`json:"check_in"`
	DateBegin 	time.Time		`gorm:"not null" json:"date_begin"`
	DateEnd		time.Time		`gorm:"not null" json:"date_end"`
	InvoiceId	*int32			`json:"invoice_id"`
	Created		time.Time		`gorm:"not null" json:"created"`
	Modified	time.Time		`gorm:"not null" json:"modified"`
}

type BookingDate struct {
	Id			int64		`gorm:"primary_key" json:"id"`
	Date		time.Time  	`gorm:"not null"`
	BookingId	int32		`gorm:"not null"`
	Booking		Booking		`json:"booking"`
	RoomId		int32		`gorm:"not null" json:"room_id"`
	Room		Room		`json:"room"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
}

type RoomService struct {
	Id			int32		`gorm:"primary_key" json:"id"`
	RoomId		int32		`gorm:"not null" json:"room_id"`
	Room		Room		`json:"room"`
	BookingId	int32		`gorm:"not null" json:"booking_id"`
	Booking		Booking		`json:"booking"`
	Ref			*string		`gorm:"size=100" json:"ref"`
	Name		string		`gorm:"size=200;not null" json:"name"`
	Price		float32		`gorm:"not null" json:"price"`
	Units		float32		`gorm:"not null" json:"units"`
	Total		float32		`gorm:"not null" json:"total"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
}

type Guest struct {
	Id			int32				`gorm:"primary_key" json:"id"`
	BookingId	int32				`gorm:"not null" json:"booking_id"`
	FirstName			string 		`gorm:"size=100;not null" json:"first_name"`
	LastName			string		`gorm:"size=100" json:"last_name"`
	SecondLastName		*string		`gorm:"size=100" json:"second_last_name"`
	BirthDate			time.Time	`json:"birth_date"`
	Document			string		`gorm:"size=50;" json:"document"`
	DocumentType		string		`gorm:"size=1" json:"document_type"`
	Gender				string		`gorm:"size=1" json:"gender"`
	Country				string		`gorm:"size=100" json:"country"`
	DocumentDate		time.Time	`json:"document_date"`
	CheckIn				time.Time	`json:"check_in"`
	Created				time.Time	`gorm:"not null" json:"created"`
	Modified			time.Time	`gorm:"not null" json:"modified"`
}
