package model

import "time"

type Client struct {
	Id					int32		`gorm:"primary_key" json:"id"`
	FirstName			string 		`gorm:"size=100;not null" json:"first_name"`
	LastName			*string		`gorm:"size=100" json:"last_name"`
	SecondLastName		*string		`gorm:"size=100" json:"second_last_name"`
	BirthDate			*time.Time	`json:"birth_date"`
	Document			*string		`json:"size=50;" json:"document"`
	DocumentType		*string		`gorm:"size=1" json:"document_type"`
	Gender				*string		`gorm:"size=1" json:"gender"`
	Address				*string		`gorm:"size=150" json:"address"`
	PostalCode			*string		`gorm:"size=10" json:"postal_code"`
	Country				*string		`gorm:"size=100" json:"country"`
	Region				*string		`gorm:"size=100" json:"region"`
	City				*string		`gorm:"size=100" json:"city"`
	HomePhone			*string		`gorm:"size=25" json:"home_phone"`
	CellPhone			*string		`gorm:"size=25" json:"cell_phone"`
	Email				*string		`gorm:"size=100" json:"email"`
	CreditCardNumber	*string		`gorm:"size=30" json:"credit_card_number"`
	CreditCardMonth		*string		`gorm:"size=2" json:"credit_card_month"`
	CreditCardYear		*string		`gorm:"size=4" json:"credit_card_year"`
	CreditCardSecurity	*string		`gorm:"size=3" json:"credit_card_security"`
	Observations		*string		`gorm:"size=999999" json:"observations"`
	Provisional			bool		`gorm:"default=false" json:"provisional"`
	Created				time.Time	`gorm:"not null" json:"created"`
	Modified			time.Time	`gorm:"not null" json:"modified"`
	FullName			string		`gorm:"size:250" json:"full_name"`
	DocumentDate		*time.Time	`json:"document_date"`
}
