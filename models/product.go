package model

import "time"

type Product struct {
	Id			int32		`gorm:"primary_key"`
	Ref			string		`gorm:"size=100;not null" json:"ref"`
	Name		string		`gorm:"size=200;not null" json:"name"`
	Price		float32		`gorm:"not null" json:"price"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
}
