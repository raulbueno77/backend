package model

import "time"

type UserGroups struct {
	Id			int32		`gorm:"primary_key" json:"id"`
	Name		string		`gorm:"size:64;not null" json:"name"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
}
