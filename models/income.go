package model

import "time"

type Income struct {
	Id				int32		`gorm:"primary_key" json:"id"`
	Date			time.Time		`gorm:"not null" json:"date"`
	PaymentMethodId	int32			`gorm:"not null" json:"payment_method_id"`
	PaymentMethod	PaymentMethod	`json:"payment_method"`
	Description		string			`gorm:"size:999999; not null" json:"description"`
	Amount			float32			`gorm:"not null" json:"amount"`
	SaleInvoiceId	*int32			`json:"sale_invoice_id"`
	BankId			int32			`json:"bank_id"`
	Bank			Bank			`json:"bank"`
	Created			time.Time		`gorm:"not null" json:"created"`
	Modified		time.Time		`gorm:"not null" json:"modified"`
}
