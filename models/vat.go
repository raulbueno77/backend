package model

type Vat struct {
	Id		int32	`gorm:"primary_key;not null" json:"id"`
	Name	string	`gorm:"size=100;not null" json:"name"`
	Value	float32	`gorm:"size=100;not null" json:"value"`
}
