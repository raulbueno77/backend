package model

import "time"

type Bank struct {
	Id			int32		`gorm:"primary_key" json:"id"`
	Name		string		`gorm:"size=74;not null" json:"name"`
	Cash		bool		`json:"cash"`
	Number		*string		`gorm:"size=75" json:"number"`
	Created		time.Time	`json:"created"`
	Modified	time.Time	`json:"modified"`
}