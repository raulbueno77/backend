package model

import "time"

type Room struct {
	Id			int32			`gorm:"primary_key" json:"id"`
	Name		string			`gorm:"size:100;not null" json:"name"`
	Floor		string			`gorm:"size:25;not null" json:"floor"`
	Attributes	[]*Attribute	`gorm:"many2many:attributes_room;"`
	TypeId		int32			`gorm:"not null"`
	Type		RoomType		`json:"type"`
	HotelId		int32			`gorm:"not null"`
	Hotel		Hotel			`json:"hotel"`
	//Prices		[]*RoomPrice	`gorm:"foreignkey=IdRoom" json:"prices"`
	Disabled	bool			`gorm:"default=false"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
}

type RoomPicture struct {
	Id			int32		`gorm:"primary_key" json:"id"`
	File		string		`gorm:"size:100;not null", json:"file"`
	AltText		*string 	`gorm:"size:100" json:"alt_text"`
	RoomTypeId	int32		`gorm:"not null"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
	Main		bool		`gorm:"not null" json:"main"`
}

type RoomPrice struct {
	Id			int64		`gorm:"primary_key" json:"id"`
	Date		time.Time	`gorm:"not null" json:"date"`
	Price		float32		`gorm:"not null" json:"price"`
	RoomTypeId	int32		`gorm:"not null", json:"room_type_id"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
}

type RoomType struct {
	Id			int32			`gorm:"primary_key" json:"id"`
	Name		string			`gorm:"size:100;not null" json:"name"`
	Pax			int32			`json:"pax"`
	Pictures	[]*RoomPicture	`gorm:"foreignkey=IdRoomType" json:"pictures"`
	DisabledWeb	bool			`gorm:"default=false" json:"disabled_web"`
	HotelId		*int32			`json:"hotel_id"`
	Hotel		*Hotel			`json:"hotel"`
	BasePrice	float32 		`gorm:"not null" json:"base_price"`
	Created		time.Time		`gorm:"not null" json:"created"`
	Modified	time.Time		`gorm:"not null" json:"modified"`
}
