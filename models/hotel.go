package model

import "time"

type Hotel struct {
	Id			int32		`gorm:"primary_key" json:"id"`
	Name		string		`gorm:"size:100;not null" json:"name"`
	Document	string		`gorm:"size:25" json:"document"`
	Reference	string		`gorm:"size:50" json:"reference"`
	Phone		string		`gorm:"size:25" json:"phone"`
	Email		string		`gorm:"size:100" json:"email"`
	Address		string		`gorm:"size:150" json:"address"`
	PostalCode	string		`gorm:"size:10" json:"postal_code"`
	Country		string		`gorm:"size:100" json:"country"`
	Region		string		`gorm:"size:100" json:"region"`
	City		string		`gorm:"size:100" json:"city"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
	Logo		*string		`gorm:"size=100" json:"logo"`
}