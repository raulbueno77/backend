package model

import "time"

type SaleInvoice struct {
	Id				int32			`gorm:"primary_key" json:"id"`
	ClientId		int32			`gorm:"not null" json:"client_id"`
	Client			Client			`json:"client"`
	FirstName			string 		`gorm:"size=100;not null" json:"first_name"`
	LastName			*string		`gorm:"size=100" json:"last_name"`
	SecondLastName		*string		`gorm:"size=100" json:"second_last_name"`
	Document			string		`json:"size=50;not null" json:"document"`
	Address				*string		`gorm:"size=150" json:"address"`
	PostalCode			*string		`gorm:"size=10" json:"postal_code"`
	Country				*string		`gorm:"size=100" json:"country"`
	Region				*string		`gorm:"size=100" json:"region"`
	City				*string		`gorm:"size=100" json:"city"`
	HomePhone			*string		`gorm:"size=25" json:"home_phone"`
	CellPhone			*string		`gorm:"size=25" json:"cell_phone"`
	Email				*string		`gorm:"size=100" json:"email"`
	Observations	*string			`gorm:"size=9999999" json:"observations"`
	Date			time.Time		`gorm:"not null" json:"date"`
	Total			float32			`gorm:"not null" json:"total"`
	Number			int32			`gorm:"not null" json:"number"`
	SerialInvoiceId	*int32			`json:"serial_invoice_id"`
	SerialInvoice	*SerialInvoice `json:"serial_invoice"`
	Created			time.Time	`gorm:"not null" json:"created"`
	Modified		time.Time	`gorm:"not null" json:"modified"`
}

type SaleInvoiceDetail struct {
	Id				int32		`gorm:"primary_key" json:"id"`
	SaleInvoiceId	int32		`gorm:"not null" json:"sale_invoice_id"`
	Reference		*string		`gorm:"size=50" json:"reference"`
	Description		string		`gorm:"size=150;not null" json:"description"`
	Units			float32		`gorm:"not null" json:"units"`
	Tax				float32		`gorm:"not null" json:"tax"`
	Price			float32		`gorm:"not null" json:"price"`
	Total			float32		`gorm:"not null" json:"total"`
}

type SerialInvoice struct {
	Id		int32		`gorm:"primary_key" json:"id"`
	Name	string		`gorm:"size=155;not null" json:"name"`
	Serial	string		`gorm:"size=50;not null" json:"serial"`
	Record	bool		`json:"record"`
}
