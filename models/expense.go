package model

import "time"

type ExpenseType struct {
	Id			int32		`gorm:"primary_key" json:"id"`
	Name		string		`gorm:"size:150; not null" json:"name"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
}

type Expense struct {
	Id				int32			`gorm:"primary_key" json:"id"`
	Date			time.Time		`gorm:"not null" json:"date"`
	PaymentMethodId	int32			`gorm:"not null" json:"payment_method_id"`
	PaymentMethod	PaymentMethod	`json:"payment_method"`
	ExpenseTypeId	int32			`gorm:"not null" json:"expense_type_id"`
	ExpenseType		ExpenseType		`json:"expense_type"`
	Description		string			`gorm:"size:999999; not null" json:"description"`
	Amount			float32			`gorm:"not null" json:"amount"`
	BankId			int32			`json:"bank_id"`
	Bank			Bank			`json:"bank"`
	Created			time.Time		`gorm:"not null" json:"created"`
	Modified		time.Time		`gorm:"not null" json:"modified"`
}
