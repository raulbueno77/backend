package model

import "time"

type Provider struct {
	Id					int32		`gorm:"primary_key" json:"id"`
	Name				string 		`gorm:"size=255;not null" json:"name"`
	Document			*string		`json:"size=50;" json:"document"`
	Address				*string		`gorm:"size=150" json:"address"`
	PostalCode			*string		`gorm:"size=10" json:"postal_code"`
	Country				*string		`gorm:"size=100" json:"country"`
	Region				*string		`gorm:"size=100" json:"region"`
	City				*string		`gorm:"size=100" json:"city"`
	JobPhone			*string		`gorm:"size=25" json:"job_phone"`
	CellPhone			*string		`gorm:"size=25" json:"cell_phone"`
	Email				*string		`gorm:"size=100" json:"email"`
	Observations		*string		`gorm:"size=999999" json:"observations"`
	Created				time.Time	`gorm:"not null" json:"created"`
	Modified			time.Time	`gorm:"not null" json:"modified"`
}
