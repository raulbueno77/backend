package model

import (
	"golang.org/x/crypto/bcrypt"
	"time"
)

type Users struct {
	Id			int32		`gorm:"primary_key" json:"id"`
	Username	string		`gorm:"size:64;not null;unique" json:"username"`
	Firstname	string		`gorm:"size:128;not null" json:"firstname"`
	Lastname	string		`gorm:"size:128;not null" json:"lastname"`
	Password	string		`gorm:"size:256;not null" json:"password"`
	Email 		string		`gorm:"size:128;not null;unique" json:"email"`
	Archived	bool		`gorm:"not null" json:"archived"`
	UserGroupId	int32		`gorm:"not null" json:"user_group_id"`
	UserGroup	UserGroups	`json:"user_group"`
	Created		time.Time	`gorm:"not null" json:"created"`
	Modified	time.Time	`gorm:"not null" json:"modified"`
	Expiration	*time.Time	`json:"expiration"`
	Description	string		`gorm:"size:999999" json:"description"`
	Hotels		[]*Hotel	`gorm:"many2many:user_hotels;"`
}

func (u *Users) SetPassword(p string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(p), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.Password = string(bytes)
	return nil
}

func (u *Users) CheckPassword(p string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(p))
	return err == nil
}

func (u *Users) QueryListCount(group string, active string) string {
	sql := "SELECT COUNT(u.id) as num FROM users WHERE 1 = 1"
	if group != "" {
		sql += " AND g.id = " + group
	}

	if active != "" {
		if active == "0" {
			active = "false"
		}
		if active == "1" {
			active = "true"
		}
		sql += " AND u.archived = " + active
	}

	return sql
}

func (u *Users) QueryList (group string, active string) string {
	sql := "SELECT u.id, g.name as \"group\", u.lastname, u.firstname, u.username, u.email, u.expiration, u.archived FROM  users u INNER JOIN user_groups g ON u.user_group_id = g.id WHERE 1 = 1"

	if group != "" {
		sql += " AND g.id = " + group
	}

	if active != "" {
		if active == "0" {
			active = "false"
		}
		if active == "1" {
			active = "true"
		}
		sql += " AND u.archived = " + active
	}

	return sql
}

type UsersList struct {
	Id			int32
	Group		string
	Firstname	string
	Lastname	string
	Username	string
	Email		string
	Expiration	*time.Time
	Archived	bool
}
